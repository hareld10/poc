from imports import *
import unet_5
from Model import Model
import os 


class CameraRoad(Model):
    def __init__(self, weights=None):
        super().__init__()
        self.thresh = 0.2
        # Running avg sequence length

        self.running_avg_len = 1
        self.running_avg = []
        
        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/weights/CRoadSeg.v1.2.unet5_best_model.pth"
        return

    def init(self, outputShape):
        self.outputShape = outputShape
        self.model = unet_5.unet()
        self.model.load_state_dict(torch.load(self.weights, map_location=lambda storage, loc: storage))
        self.model = self.model.cuda(self.gpus_list[0])
        self.model.eval()
        print("Init CameraRoadModel - Weights ", self.weights)
        return

    def getRaw(self, im):
        processed_im = torch.from_numpy(im).permute(2, 0, 1).float()
        with torch.no_grad():
            modelInput = torch.unsqueeze(Variable(processed_im).cuda(self.gpus_list[0]), 0)
            pred = torch.squeeze(self.model(modelInput))
            probs = F.sigmoid(pred).cpu().numpy()
        return probs

    def getIm(self, im, imToDraw):
        probs = self.getRaw(im)
        
        # Running avg
        self.running_avg.append(probs)
        if len(self.running_avg) > self.running_avg_len:
            self.running_avg.pop(0)

        probs_running_avg = np.zeros(probs.shape)
        for i in range(len(self.running_avg)):
            probs_running_avg += self.running_avg[i]

        probs_running_avg /= len(self.running_avg)
        
        probsImg = np.stack([probs_running_avg,probs_running_avg,probs_running_avg],axis=2)
        heatMap  = np.uint8(exposure.rescale_intensity(np.copy(probs_running_avg), out_range=(255, 0)))
        heatMap  = cv2.applyColorMap(heatMap, cv2.COLORMAP_JET)
        heatMap  = cv2.addWeighted(heatMap, 0.4, imToDraw, 0.6, 0)
        heatMap  = np.where(probsImg>self.thresh,heatMap,imToDraw)
        return heatMap, probs_running_avg


