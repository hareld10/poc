import pandas as pd
import glob
import cv2
import numpy as np
import torch
print("torch version", torch.__version__)
from AI_Sockets.classes import *
import matplotlib.pyplot as plt
from Manager import Manager

m = Manager()
outputShape = (int(940 // 5), int(1824 // 5))
m.init(outputShape)

# base = "/workspace/HDD/200604/200604_drive_1_W36_M8_RF1_ANT3/"
base = "/mnt/hdd/200624/200624_drive_1_W36_M8_RF1_ANT3/"
df = base + "filteredData.csv"
df = pd.read_csv(df)
current = df.sample(20).reset_index()

for idx in range(len(current)):
    print(idx)
    rgbImPath = base + "/rgbCamera/rgbCamera_" + str(current.iloc[idx]["timestampCamera"]) + ".png"
    pathRadar = base + "/S_ANT/S_ANT_" + str(current.iloc[idx]["timestampRadar"]) + ".npy"

    rgbImg = cv2.imread(rgbImPath)
    S_ANT = np.load(pathRadar)

    state = AIState()

    # state.screens = ["Radar Road Vehicle", "Camera", "Fusion", "Range Doppler"]
    # state.screens = ["Radar Road Vehicle", "Range Doppler", None,  None]
    # state.screens = ["Combined RV", "Range Doppler", None, None]
    state.screens = ["Radar Ped", "Range Doppler", None, None]
    # state.screens = ["Radar Road Vehicle", "Range Azimuth", None, None]
    # state.screens = ["Radar Perception", "Range Doppler", None, None]

    state.camera_off = False
    state.od_visualization = 1
    state.seg_instance = False
    state.od_flag = True

    state.thresh = {}
    state.thresh["Camera Road"] = 0.6
    state.thresh["Radar Road"] = 0.5
    state.thresh["Radar Vehicle"] = 0.6
    state.thresh["Veh Peak"] = 0.6
    state.running_avg = {"Radar": 1, "Camera": 1}

    # peaks_thresh = [0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
    # nms_thresh = [0.15, 0.2, 0.25, 0.3, 0.35]
    # for p in peaks_thresh:
    #     for n in nms_thresh:

    im = m.getIm(state, S_ANT, rgbImg)

    plt.figure(figsize=(20,10))
    plt.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
    plt.savefig("./images/" + str(idx) +".png")
    plt.close()
    # plt.show()