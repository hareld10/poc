import os
import pickle
from multiprocessing import Process, Array, Lock
from queue import Queue
import numpy as np
import time
from tqdm import tqdm


class TemporalModule(object):
    def __init__(self):
        self.max_size = 20
        self.queue = Queue(maxsize=self.max_size)
        self.dict = {}
        self.window_size = 10
        self.num_frames = 8
        self.stop_flag = False

    def add_features(self, x_t, timestamp):
        print("TemporalModule: add_features", x_t.shape, timestamp)
        if self.queue.full():
            self.queue.get()
        self.queue.put((x_t, timestamp))
        return

    def close(self):
        self.stop_flag = True

    def refresh_queue(self, timestamp):
        to_append = Queue(maxsize=self.max_size)
        ret = []
        while not self.queue.empty():
            x = self.queue.get(block=False)
            diff = abs(timestamp - x[1])
            if not diff >= self.window_size:
                to_append.put(x)
                ret.append(x[0])
            else:
                print("Removed from queue", diff)
        self.queue = to_append
        return np.array(ret)

    def get_features(self, timestamp, ):
        # Ensure queue holds relevant elements
        queue_values = self.refresh_queue(timestamp)
        if len(queue_values) == 0:
            print("Couldn't Find Match")
            return None
        # Draw evenly spaced timestamps
        indices = np.round(np.linspace(0, len(queue_values) - 1, self.num_frames)).astype(int)
        # print("todo boom", indices, "len(queue_values)", len(queue_values), "queue_values.shape", queue_values.shape)
        to_return = np.concatenate(queue_values[indices], axis=1)
        # print("TemporalModule: to_return ", to_return.shape)
        return to_return

    def run(self, connections):
        print("Init Temporal Modules", connections)
        self.stop_flag = False
        while not self.stop_flag:
            for k, v in connections.items():
                parent_conn, child_conn = v
                if parent_conn.poll():
                    msg = pickle.loads(parent_conn.recv_bytes())
                    if msg[0] == "ts":
                        ts = msg[1][0]
                        x_t = self.get_features(ts)
                        msg_to_send = np.array(["x_t", x_t])
                        parent_conn.send_bytes(pickle.dumps(msg_to_send))
                    elif msg[0] == "x_t":
                        if msg[1] is not None:
                            self.add_features(msg[1], msg[2][0])
            time.sleep(0.005)


# Test
class Runner:
    def __init__(self):
        self.t_m = TemporalModule()
        self.num_processes = 2
        self.lock = Lock()
        self.processes = {}
        return

    def run(self):
        for i in range(self.num_processes):
            self.processes[i] = Process(target=self.manager, args=(self.t_m, self.lock,))
            self.processes[i].start()

    def manager(self, temporal_module, mutex):
        print("started", os.getpid())
        count = 1
        for i in tqdm(range(1, 100000)):
            time.sleep(0.01)
            x_t = np.array([count, count + 1])
            count += 1
            ts = time.time()

            temporal_module.add_features(x_t, ts)

            if i % 10 == 0:
                # print("ts", ts)
                temporal_module.get_features(ts)
                # print()

        print("finished")
