from imports import *
import radar_veh_net_v2
from Model import Model


class RadarVehNet(Model):
    def __init__(self, road=False, vehicle=True, weights=None):
        super().__init__()
        self.thresh = 0.6
        self.alpha = 0.4
        self.peak_thresh_veh = 0.4
        self.nms_thresh_veh = 0.35
        self.bb_thresh_veh = 0.25
        self.show_heatmap = False
        self.colorPred = (255, 0, 0)

        # Running avg sequence length
        self.running_avg_flag = False
        self.running_avg_len = 1
        self.running_avg = []

        self.centers_pred_veh = None
        self.mode_dict = {"weights": "/weights/radar_percep_rvp_st.v3.1.leopard_radar_veh_net_v2_best_model.pth"}

        self.mode = "vehicle"
        self.row_thickness = 2
        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]

    def model_init(self):
        self.model.load_state_dict(torch.load(self.weights, map_location=lambda storage, loc: storage))
        self.model = self.model.cuda(self.gpus_list[0])
        self.model.eval()
        print("Model: ", self.mode, " - Weights: ", self.weights)
        return

    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size

        self.model = radar_veh_net_v2.net(output_shape=self.outputShape,
                                          gpus_list=self.gpus_list,
                                          batch_size=self.batch_size,
                                          doppler_ch=48,
                                          zero_pad=0,
                                          output_ch=5)

        self.model_init()

    def getRaw(self, S_ANT):
        s = self.process_S_ANT(S_ANT)

        with torch.no_grad():
            # modelInput = torch.empty((self.batch_size, *s.shape))
            # modelInput[0] = Variable(s)
            modelInput = torch.unsqueeze(Variable(s).cuda(self.gpus_list[0]), 0)
            # print("RadarVehNet ModelInput Shape", modelInput.shape)
            pred = self.model(modelInput)

        # print("RadarVehNet pred", pred.shape)
        pred_veh = pred[0].squeeze(0)
        pred_veh[0, :, :] = torch.sigmoid(pred_veh[0, :, :])
        pred_veh = pred_veh.cpu().numpy()
        self.centers_pred_veh = np.stack([pred_veh[0, :, :], pred_veh[0, :, :], pred_veh[0, :, :]], axis=2)
        peaks = detect_peaks(self.centers_pred_veh, self.peak_thresh_veh)
        peaks = peaks[:, :, 0] * 1
        row, col = peaks.nonzero()

        return row, col, pred_veh

    def update_state(self, state):
        if "Veh Peak" in state.thresh:
            self.peak_thresh_veh = float(state.thresh["Veh Peak"])
        if "Veh NMS" in state.thresh:
            self.nms_thresh_veh = float(state.thresh["Veh NMS"])

    def getIm(self, array, imToDraw, state, scale):
        self.update_state(state)
        row, col, pred_veh = self.getRaw(array)
        scale_x, scale_y = scale


        # show centers
        # for i in range(len(row)):
        #     x1 = int((col[i] - 2)*scale_x)
        #     x2 = int((col[i] + 2)*scale_x)
        #     y1 = int((row[i] - 2)*scale_y)
        #     y2 = int((row[i] + 2)*scale_y)
        #     if self.state.od_visualization == 0:
        #         cv2.rectangle(imToDraw, (x1, y1), (x2, y2), (255, 0, 0), 2)
        #
        #     if i == 100: break

        # NMS
        bb = box_nms(row, col, pred_veh, self.nms_thresh_veh, self.bb_thresh_veh, 'veh')

        if self.show_heatmap:
            self.centers_pred_veh = cv2.resize(self.centers_pred_veh, (imToDraw.shape[1],imToDraw.shape[0]))
            heatMapRadar = np.uint8(exposure.rescale_intensity(self.centers_pred_veh, out_range=(255, 0)))
            heatMapRadar = cv2.applyColorMap(heatMapRadar, cv2.COLORMAP_JET)
            heatMapRadar = cv2.addWeighted(heatMapRadar, 0.4, imToDraw, 0.6, 0)
            imToDraw = np.where(self.centers_pred_veh > 0.4, heatMapRadar, imToDraw)

        # Plot bb
        for i in range(bb.shape[0]):
            x1 = int((bb[i, 0])*scale_x)
            y1 = int((bb[i, 1])*scale_y)
            x2 = int((bb[i, 2])*scale_x)
            y2 = int((bb[i, 3])*scale_y)
            if state.od_visualization == 0:
                cv2.rectangle(imToDraw, (x1, y1), (x2, y2), (0, 255, 0), 2)
            elif state.od_visualization == 1:
                cx = (x2 - x1) // 2 + x1
                cy = (y2 - y1) // 2 + y1
                imToDraw = cv2.arrowedLine(imToDraw, (cx, cy), (cx, y1), color=(0, 255, 0), thickness=self.row_thickness)
                imToDraw = cv2.arrowedLine(imToDraw, (cx, cy), (x2, cy), color=(0, 255, 0), thickness=self.row_thickness)
                imToDraw = cv2.arrowedLine(imToDraw, (cx, cy), (x1, cy), color=(0, 255, 0), thickness=self.row_thickness)
                imToDraw = cv2.arrowedLine(imToDraw, (cx, cy), (cx, y2), color=(0, 255, 0), thickness=self.row_thickness)

            if i == 100: break

        return imToDraw, bb

        # probs = self.getRaw(array)
        # Running avg

        # if self.running_avg_flag:
        #     probs = self.apply_running_avg(probs)
        #
        # probs_running_avg = probs
        #
        # # print("Radar: Running Avg Took: {:.4f}".format((time.time() - curStart)))
        # # HeatMap
        #  = np.stack([probs_running_avg, probs_running_avg, probs_running_avg], axis=2)
        # heatMapRadar = np.uint8(exposure.rescale_intensity(np.copy(probs_running_avg), out_range=(255, 0)))
        # heatMapRadar = cv2.applyColorMap(heatMapRadar, cv2.COLORMAP_JET)
        # if self.camera_bg:
        #     heatMapRadar = cv2.addWeighted(heatMapRadar, 0.4, imToDraw, 0.6, 0)
        #     heatMapRadar = np.where(probsImgRadar > self.thresh, heatMapRadar, imToDraw)
        # else:
        #     heatMapRadar = np.where(probsImgRadar > self.thresh, heatMapRadar, np.zeros(imToDraw.shape, dtype=np.uint8))
        # return heatMapRadar, probs_running_avg
