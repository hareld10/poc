# # %matplotlib inline
from IPython.core.display import display, HTML

display(HTML("<style>.container { width:95% !important; }</style>"))
import torch
import torch.nn as nn
import torch.nn.functional as F


# +
# Coordinated convolution: https://arxiv.org/pdf/1807.03247.pdf
# https://github.com/Wizaron/coord-conv-pytorch/blob/master/coord_conv.py

class add_coord(nn.Module):

    def __init__(self, image_shape, gpus_list, batch_size):
        super(add_coord, self).__init__()

        image_height = image_shape[0]
        image_width = image_shape[1]

        #         y_coords = (2.0 * torch.arange(image_height).unsqueeze(1).expand(image_height, image_width) / (image_height - 1.0) - 1.0)# * image_height//2
        self.x_coords = torch.arange(image_width).unsqueeze(0).expand(image_height, image_width).float() / image_width
        self.y_coords = torch.arange(image_height).unsqueeze(1).expand(image_height, image_width).float() / image_height

        self.coords = torch.unsqueeze(torch.stack((self.y_coords, self.x_coords), dim=0), dim=0).repeat(batch_size, 1,
                                                                                                        1, 1)
        self.coords = self.coords.cuda(gpus_list[0])

        self.batch_size = batch_size
        self.gpus_list = gpus_list

    def forward(self, x):

        #         print('x',x.shape)
        #         print('self.coords',self.coords.shape)

        if x.shape[0] != self.batch_size:
            coords = torch.unsqueeze(torch.stack((self.y_coords, self.x_coords), dim=0), dim=0).repeat(x.shape[0], 1, 1,
                                                                                                       1).cuda(
                self.gpus_list[0])
        else:
            coords = self.coords

        x = torch.cat((x, coords), dim=1)

        return x


# +
# Duel Attention: https://arxiv.org/pdf/1809.02983.pdf
# https://github.com/junfu1115/DANet/blob/master/encoding/nn/attention.py

class CAM_Module(nn.Module):
    """ Channel attention module"""

    def __init__(self, in_dim, batch_size):
        super(CAM_Module, self).__init__()
        self.chanel_in = in_dim
        self.gamma = nn.Parameter(torch.ones(1))
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        """
            inputs :
                x : input feature maps( B X C X H X W)
            returns :
                out : attention value + input feature
                attention: B X C X C
        """
        m_batchsize, C, height, width = x.size()
        proj_query = x.view(m_batchsize, C, -1)
        proj_key = x.view(m_batchsize, C, -1).permute(0, 2, 1)
        energy = torch.bmm(proj_query, proj_key)
        energy_new = torch.max(energy, -1, keepdim=True)[0].expand_as(energy) - energy
        attention = self.softmax(energy_new)
        proj_value = x.view(m_batchsize, C, -1)

        out = torch.bmm(attention, proj_value)
        out = out.view(m_batchsize, C, height, width)

        #         print('CAM gamma',self.gamma)
        #         print('CAM out-x',self.gamma*out)

        return self.gamma * out + x


# +
class depthwise_separable_conv(nn.Module):
    def __init__(self, in_ch, out_ch, kernels_per_layer=1):
        super(depthwise_separable_conv, self).__init__()
        self.depthwise = nn.Conv2d(in_ch, in_ch * kernels_per_layer, kernel_size=3, padding=1, groups=in_ch)
        self.pointwise = nn.Conv2d(in_ch * kernels_per_layer, out_ch, kernel_size=1)

    def forward(self, x):
        out = self.depthwise(x)
        out = self.pointwise(out)
        return out


class depthwise_separable_conv_3d(nn.Module):
    def __init__(self, in_ch, out_ch, kernels_per_layer=1):
        super(depthwise_separable_conv_3d, self).__init__()
        self.depthwise = nn.Conv3d(in_ch, in_ch * kernels_per_layer, kernel_size=3, padding=1, groups=in_ch)
        self.pointwise = nn.Conv3d(in_ch * kernels_per_layer, out_ch, kernel_size=1)

    def forward(self, x):
        out = self.depthwise(x)
        out = self.pointwise(out)
        return out


# +
# Unet: https://github.com/milesial/Pytorch-UNet/tree/master/unet

class double_conv(nn.Module):
    def __init__(self, in_ch, out_ch, batch_size, kernel_size=3, padding=1):
        super(double_conv, self).__init__()
        self.conv1 = nn.Sequential(
            CAM_Module(in_ch, batch_size),
            nn.Conv2d(in_ch, out_ch, kernel_size=kernel_size, padding=padding),
            nn.InstanceNorm2d(out_ch, affine=True),
            nn.LeakyReLU())
        self.conv2 = nn.Sequential(
            CAM_Module(out_ch + in_ch, batch_size),
            nn.Conv2d(out_ch + in_ch, out_ch, 3, padding=1),
            nn.InstanceNorm2d(out_ch, affine=True),
            nn.LeakyReLU())

    def forward(self, x):
        x = self.conv2(torch.cat([self.conv1(x), x], dim=1))
        return x


class inconv(nn.Module):
    def __init__(self, in_ch, out_ch, batch_size):
        super(inconv, self).__init__()
        #         self.conv = double_conv(in_ch, out_ch,batch_size)
        self.conv = double_conv2D(in_ch, out_ch, batch_size=batch_size, angular_flag=False)

    def forward(self, x):
        x = self.conv(x)
        return x


class down(nn.Module):
    def __init__(self, in_ch, out_ch, batch_size):
        super(down, self).__init__()
        self.mpconv = nn.Sequential(
            nn.AvgPool2d(2),
            #             double_conv(in_ch, out_ch,batch_size)
            double_conv2D(in_ch, out_ch, batch_size=batch_size, angular_flag=False))

    def forward(self, x):
        x = self.mpconv(x)
        return x


class up(nn.Module):
    def __init__(self, in_ch, out_ch, batch_size, bilinear=False):
        super(up, self).__init__()

        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        else:
            self.up = nn.ConvTranspose2d(in_ch // 2, in_ch // 2, 2, stride=2)

        #         self.conv = double_conv(in_ch, out_ch,batch_size)

        self.conv = double_conv2D(in_ch, out_ch, batch_size=batch_size, angular_flag=False)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        diffX = x1.size()[2] - x2.size()[2]
        diffY = x1.size()[3] - x2.size()[3]

        if diffX < 0:
            diffX = abs(diffX)
            if diffY < 0:
                diffY = abs(diffY)
            x1 = F.pad(x1, (diffY // 2, diffY - diffY // 2, diffX // 2, diffX + diffX // 2))
        elif diffX > 0:
            x2 = F.pad(x2, (diffY // 2, diffY - diffY // 2, diffX // 2, diffX - diffX // 2))
        elif diffX == 0:  # and diffY>0:
            x2 = F.pad(x2, (diffY // 2, diffY - diffY // 2, diffX // 2, diffX - diffX // 2))
        x = torch.cat([x2, x1], dim=1)
        x = self.conv(x)
        return x


class outconv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(outconv, self).__init__()
        self.conv = nn.Conv2d(in_ch, out_ch, 1)

    def forward(self, x):
        x = self.conv(x)
        return x


class double_conv3D(nn.Module):
    '''(conv => norm => act) * 2'''

    def __init__(self, in_ch, out_ch):
        super(double_conv3D, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv3d(in_ch, out_ch, 3, padding=1),
            nn.InstanceNorm3d(out_ch, affine=True),
            nn.LeakyReLU(),
            nn.Conv3d(out_ch, out_ch, 3, padding=1),
            nn.InstanceNorm3d(out_ch, affine=True),
            nn.LeakyReLU())

        self.rng_down = nn.AvgPool3d(kernel_size=(2, 1, 1))
        self.angular_up = nn.ConvTranspose3d(out_ch, out_ch, kernel_size=(1, 2, 2), stride=(1, 2, 2))

    def forward(self, x):
        x = self.conv(x)
        x = self.rng_down(x)
        x = self.angular_up(x)
        return x


class conv3D_down_rng(nn.Module):
    '''(conv => norm => act) * 2'''

    def __init__(self, in_ch, out_ch, down_kernel=(3, 1, 1), angular_flag=False, up_kernel=(1, 2, 2),
                 up_stride=(1, 2, 2), up_padding=(0, 0, 0)):
        super(conv3D_down_rng, self).__init__()
        self.conv = nn.Sequential(
            depthwise_separable_conv_3d(in_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm3d(out_ch, affine=True),
            nn.LeakyReLU(),
            depthwise_separable_conv_3d(out_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm3d(out_ch, affine=True),
            nn.LeakyReLU())

        self.rng_down = nn.MaxPool3d(kernel_size=down_kernel)

        self.angular_flag = angular_flag
        if angular_flag:
            self.angular_up_3d = nn.ConvTranspose3d(out_ch, out_ch, kernel_size=up_kernel, stride=up_stride,
                                                    padding=up_padding)

    def forward(self, x):
        x = self.conv(x)

        if self.angular_flag:
            x = self.angular_up_3d(x)

        x = self.rng_down(x)

        return x


class double_conv2D(nn.Module):
    '''(conv => norm => act) * 2'''

    def __init__(self, in_ch, out_ch, padding=0, kernel_size=2, stride=2, batch_size=1, output_padding=0,
                 angular_flag=True):
        super(double_conv2D, self).__init__()
        self.conv = nn.Sequential(
            depthwise_separable_conv(in_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm2d(out_ch, affine=True),
            nn.LeakyReLU(),
            depthwise_separable_conv(out_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm2d(out_ch, affine=True),
            nn.LeakyReLU())

        if angular_flag:
            self.angular_up = nn.ConvTranspose2d(out_ch, out_ch, kernel_size=kernel_size, stride=stride,
                                                 padding=padding, output_padding=output_padding)

        self.CAM = nn.Sequential(CAM_Module(out_ch, batch_size),
                                 nn.Conv2d(out_ch, out_ch, 3, padding=1, bias=True),
                                 nn.InstanceNorm2d(out_ch, affine=True),
                                 nn.LeakyReLU())

        self.angular_flag = angular_flag

    def forward(self, x):
        x = self.conv(x)
        if self.angular_flag:
            x = self.angular_up(x)
        x = self.CAM(x)
        return x


# -


class radar_encoder(nn.Module):
    def __init__(self, gpus_list, doppler_ch, batch_size):
        super(radar_encoder, self).__init__()

        self.conv1 = conv3D_down_rng(doppler_ch * 2, 128)
        self.conv2 = conv3D_down_rng(doppler_ch * 2 + 128, 128)
        self.conv3 = conv3D_down_rng(doppler_ch * 2 + 128 * 2, 128)
        self.conv4 = conv3D_down_rng(doppler_ch * 2 + 128 * 3, 128, angular_flag=True, up_kernel=(1, 2, 1),
                                     up_stride=(1, 2, 1), up_padding=(0, 0, 0))
        self.conv5 = conv3D_down_rng(doppler_ch * 2 + 128 * 4, 128, angular_flag=True, up_kernel=(1, 2, 2),
                                     up_stride=(1, 2, 2), up_padding=(0, 0, 0))

    def forward(self, x):
        x1 = self.conv1(x)
        x2 = self.conv2(torch.cat([F.interpolate(x, x1.shape[-3:]), x1], dim=1))
        x3 = self.conv3(torch.cat([F.interpolate(x, x2.shape[-3:]), F.interpolate(x1, x2.shape[-3:]), x2], dim=1))
        x4 = self.conv4(torch.cat(
            [F.interpolate(x, x3.shape[-3:]), F.interpolate(x1, x3.shape[-3:]), F.interpolate(x2, x3.shape[-3:]), x3],
            dim=1))
        x5 = self.conv5(torch.cat(
            [F.interpolate(x, x4.shape[-3:]), F.interpolate(x1, x4.shape[-3:]), F.interpolate(x2, x4.shape[-3:]),
             F.interpolate(x3, x4.shape[-3:]), x4], dim=1))

        x = torch.squeeze(x5, 2)  # torch.Size([b, 128, 6, 14])

        return x


class radar_decoder(nn.Module):
    def __init__(self, gpus_list, doppler_ch, batch_size):
        super(radar_decoder, self).__init__()

        self.conv1 = double_conv2D(128, 128, batch_size=batch_size)
        self.conv2 = double_conv2D(128 * 2, 128, batch_size=batch_size)
        self.conv3 = double_conv2D(128 * 3, 64, batch_size=batch_size, kernel_size=(1, 2), stride=(1, 2))
        self.conv4 = double_conv2D(128 * 3 + 64, 64, batch_size=batch_size, angular_flag=False)

    def forward(self, x):
        x1 = self.conv1(x)
        x2 = self.conv2(torch.cat([F.interpolate(x, x1.shape[-2:]), x1], dim=1))
        x3 = self.conv3(torch.cat([F.interpolate(x, x2.shape[-2:]), F.interpolate(x1, x2.shape[-2:]), x2], dim=1))
        x4 = self.conv4(torch.cat(
            [F.interpolate(x, x3.shape[-2:]), F.interpolate(x1, x3.shape[-2:]), F.interpolate(x2, x3.shape[-2:]), x3],
            dim=1))

        return x4


class temporal_module(nn.Module):
    def __init__(self, batch_size, temporal_frames, temporal_module_type):
        super(temporal_module, self).__init__()

        self.temporal_module_type = temporal_module_type
        self.temporal_frames = temporal_frames

        if temporal_module_type == 'cnn':
            self.conv1 = double_conv2D(128 + 128 * temporal_frames, 128, batch_size=batch_size, angular_flag=False)
            self.conv2 = double_conv2D(128 + 128, 128, batch_size=batch_size, angular_flag=False)
            self.conv3 = double_conv2D(128 + 128, 128, batch_size=batch_size, angular_flag=False)
            self.conv4 = double_conv2D(128 + 128, 128, batch_size=batch_size, angular_flag=False)

        elif temporal_module_type == 'lstm':
            pass

        elif temporal_module_type == 'transformer':
            pass

    def forward(self, x, x_t, ignore_temporal_features):

        if self.temporal_module_type == 'cnn':
            if ignore_temporal_features:
                x_t = x.repeat(1, self.temporal_frames, 1, 1)

            # TODO: get difference from current features

            x1 = self.conv1(torch.cat([x, x_t], dim=1))
            x2 = self.conv2(torch.cat([x1, x], dim=1))
            x3 = self.conv3(torch.cat([x2, x1], dim=1))
            x = self.conv4(torch.cat([x3, x2], dim=1))

        elif self.temporal_module_type == 'lstm':
            pass

        elif self.temporal_module_type == 'transformer':
            pass

        return x


class task_encoder(nn.Module):
    def __init__(self, output_shape, gpus_list, batch_size, scale_factor):
        super(task_encoder, self).__init__()

        # Coordinated conv
        self.coord = add_coord(output_shape, gpus_list, batch_size)

        # Attention layers
        in_ch = 64 + 2
        self.CAM_in = nn.Sequential(CAM_Module(in_ch, batch_size), nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                    nn.InstanceNorm2d(in_ch, affine=True), nn.LeakyReLU())

        self.inc = inconv(64 + 2, int(64 / scale_factor), batch_size=batch_size)
        self.down_1 = down(int(64 / scale_factor), int(128 / scale_factor), batch_size=batch_size)
        self.down_2 = down(int(128 / scale_factor), int(256 / scale_factor), batch_size=batch_size)
        self.down_3 = down(int(256 / scale_factor), int(512 / scale_factor), batch_size=batch_size)
        self.down_4 = down(int(512 / scale_factor), int(512 / scale_factor), batch_size=batch_size)

    #         self.bifpn   = biFPN(gpus_list,batch_size,int(64/scaleFactor),int(128/scaleFactor),int(256/scaleFactor),int(512/scaleFactor),int(512/scaleFactor))

    def forward(self, x):
        x1 = self.inc(self.CAM_in(self.coord(x)))
        x2 = self.down_1(x1)
        x3 = self.down_2(x2)
        x4 = self.down_3(x3)
        x5 = self.down_4(x4)
        #         x1,x2,x3,x4,x5 = self.bifpn(x1,x2,x3,x4,x5) # BiFPN 2D

        return x1, x2, x3, x4, x5


class task_head(nn.Module):
    def __init__(self, batch_size, output_ch, scale_factor):
        super(task_head, self).__init__()

        self.up_1 = up(int(1024 / scale_factor), int(256 / scale_factor), batch_size=batch_size)
        self.up_2 = up(int(512 / scale_factor), int(128 / scale_factor), batch_size=batch_size)
        self.up_3 = up(int(256 / scale_factor), int(64 / scale_factor), batch_size=batch_size)
        self.up_4 = up(int(128 / scale_factor), int(64 / scale_factor), batch_size=batch_size)
        self.out_c = outconv(int(64 / scale_factor), output_ch)

    def forward(self, x1, x2, x3, x4, x5):
        x = self.up_1(x5, x4)
        x = self.up_2(x, x3)
        x = self.up_3(x, x2)
        x = self.up_4(x, x1)
        x = self.out_c(x)

        return x


class net(nn.Module):
    def __init__(self, output_shape, gpus_list, batch_size, doppler_ch, zero_pad, output_ch, temporal_frames,
                 temporal_module_type):
        super(net, self).__init__()

        # Radar encoder
        self.radar_enc = radar_encoder(gpus_list, doppler_ch, batch_size)

        # Temporal module
        self.temporal_module = temporal_module(batch_size, temporal_frames, temporal_module_type)

        # Radar decoder
        self.radar_dec = radar_decoder(gpus_list, doppler_ch, batch_size)

        # Task encoder
        self.task_enc = task_encoder(output_shape, gpus_list, batch_size, scale_factor=1)

        # Task head
        self.task_head = task_head(batch_size, output_ch=output_ch, scale_factor=1)

    def forward(self, x, x_t, single_frame_mode):  # in:[b, 48*2, 400, 6, 14]    out:[b,134, 260]

        # Radar encoder
        x = self.radar_enc(x)  # radar features torch.Size([temporal_frames, 32, 6, 14])

        # Temporal module
        x_t = self.temporal_module(x, x_t, single_frame_mode)

        # Radar decoder
        x = self.radar_dec(x_t)

        # Task encoder
        x1, x2, x3, x4, x5 = self.task_enc(x)

        # Task decoder
        x = self.task_head(x1, x2, x3, x4, x5)

        return x, x_t

# +
# output_shape (188, 364)
# output_shape (156, 304)
# output_shape (134, 260)

# Transpose conv

# conv_2d_4 torch.Size([8, 64, 96, 224])
# conv_2d_5 torch.Size([8, 64, 188, 364]) (2,42)

# conv_2d_4 torch.Size([8, 64, 96, 224])
# conv_2d_5 torch.Size([8, 64, 156, 304]) padding (18,72)

# stride = 2
# padding = 94
# dilation = 1
# kernel_size = 2
# output_padding = 0
# input = 224
# print((input-1)*stride-2*padding+dilation*(kernel_size-1)+output_padding+1)

# #################################

# Conv

# stride = 3
# padding = 22
# dilation = 1
# kernel_size = 3
# input = 340
# print((input+2*padding-dilation*(kernel_size-1)-1)/stride+1)
