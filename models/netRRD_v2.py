
import torch
import torch.nn as nn
import torch.nn.functional as F

##########################################################################################################################    
# Duel Attention: https://arxiv.org/pdf/1809.02983.pdf
# https://github.com/junfu1115/DANet/blob/master/encoding/nn/attention.py
##########################################################################################################################

# TODO: check which import is needed
import numpy as np
import math
from torch.nn import Module, Sequential, Conv2d, ReLU,AdaptiveMaxPool2d, AdaptiveAvgPool2d, AvgPool2d, MaxPool2d, Parameter, Linear, Sigmoid, Softmax, Dropout, Embedding
from torch.autograd import Variable

class CAM_Module(Module):
    """ Channel attention module"""
    def __init__(self, in_dim):
        super(CAM_Module, self).__init__()
        self.chanel_in = in_dim


        self.gamma = Parameter(torch.ones(1))
        self.softmax  = Softmax(dim=-1)
    def forward(self,x):
        """
            inputs :
                x : input feature maps( B X C X H X W)
            returns :
                out : attention value + input feature
                attention: B X C X C
        """
        m_batchsize, C, height, width = x.size()
        proj_query = x.view(m_batchsize, C, -1)
        proj_key = x.view(m_batchsize, C, -1).permute(0, 2, 1)
        energy = torch.bmm(proj_query, proj_key)
        energy_new = torch.max(energy, -1, keepdim=True)[0].expand_as(energy)-energy
        attention = self.softmax(energy_new)
        proj_value = x.view(m_batchsize, C, -1)

        out = torch.bmm(attention, proj_value)
        out = out.view(m_batchsize, C, height, width)
        
#         print('CAM gamma',self.gamma)
#         print('CAM out-x',self.gamma*out)


        out = self.gamma*out + x
        return out
    
##########################################################################################################################    
# Unet: https://github.com/milesial/Pytorch-UNet/tree/master/unet
##########################################################################################################################
class double_conv(nn.Module):
    '''(conv => BN => ReLU) * 2'''
    def __init__(self, in_ch, out_ch):
        super(double_conv, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_ch, out_ch, 3, padding=1),
            nn.InstanceNorm2d(out_ch,affine=True),
            nn.LeakyReLU(),
            nn.Conv2d(out_ch, out_ch, 3, padding=1),
            nn.InstanceNorm2d(out_ch,affine=True),
            nn.LeakyReLU()
        )

    def forward(self, x):
        x = self.conv(x)
        return x


class inconv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(inconv, self).__init__()
        self.conv = double_conv(in_ch, out_ch)

    def forward(self, x):
        x = self.conv(x)
        return x


class down(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(down, self).__init__()
        self.mpconv = nn.Sequential(
            nn.MaxPool2d(2),
            double_conv(in_ch, out_ch)
        )

    def forward(self, x):
        x = self.mpconv(x)
        return x


class up(nn.Module):
    def __init__(self, in_ch, out_ch, bilinear=False):
        super(up, self).__init__()

        #  would be a nice idea if the upsampling could be learned too,
        #  but my machine do not have enough memory to handle all those weights
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        else:
            self.up = nn.ConvTranspose2d(in_ch//2, in_ch//2, 2, stride=2)

        self.conv = double_conv(in_ch, out_ch)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        diffX = x1.size()[2] - x2.size()[2]
        diffY = x1.size()[3] - x2.size()[3]
#         print('diffX:',diffX)
#         print('diffY:',diffY)
#         print('x1 before:',x1.shape)
#         print('x2 before:',x2.shape)
        if diffX<0:
            diffX = abs(diffX)
            if diffY<0:
                diffY = abs(diffY)
            x1 = F.pad(x1, (diffY // 2, diffY - diffY//2, diffX // 2, diffX + diffX//2))
        elif diffX>0:
            x2 = F.pad(x2, (diffY // 2, diffY - diffY//2, diffX // 2, diffX - diffX//2))
        elif diffX==0:# and diffY>0:
            x2 = F.pad(x2, (diffY // 2, diffY - diffY//2, diffX // 2, diffX - diffX//2))
#         print('x1 after:',x1.shape)
#         print('x2 after:',x2.shape)
        x = torch.cat([x2, x1], dim=1)
        x = self.conv(x)
        return x

class outconv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(outconv, self).__init__()
        self.conv = nn.Conv2d(in_ch, out_ch, 1)

    def forward(self, x):
        x = self.conv(x)
        return x


class net(nn.Module):
    def __init__(self):
        super(net, self).__init__()
        
        numClasses  = 1
        scaleFactor = 4
        self.inc    = inconv(96,                   int(64/scaleFactor))
        self.down1  = down(int(64/scaleFactor),    int(128/scaleFactor))
        self.down2  = down(int(128/scaleFactor),   int(256/scaleFactor))
        self.down3  = down(int(256/scaleFactor),   int(512/scaleFactor))
        self.down4  = down(int(512/scaleFactor),   int(512/scaleFactor))
        self.up1    = up(int(1024/scaleFactor),    int(256/scaleFactor))
        self.up2    = up(int(512/scaleFactor),     int(128/scaleFactor))
        self.up3    = up(int(256/scaleFactor),     int(64/scaleFactor))
        self.up4    = up(int(128/scaleFactor),     int(64/scaleFactor))
        self.outc   = outconv(int(64/scaleFactor), numClasses)
        
        # Attention layers
        in_ch     = int(64/scaleFactor)
        self.CAM1 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
               
            
            
        in_ch     = int(128/scaleFactor)
        self.CAM2 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
        
        in_ch     = int(256/scaleFactor)
        self.CAM3 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
        
        in_ch     = int(512/scaleFactor)
        self.CAM4 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
        
        in_ch     = int(512/scaleFactor)
        self.CAM5 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())

        in_ch     = int(256/scaleFactor)
        self.CAM6 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
                     
        in_ch     = int(128/scaleFactor)
        self.CAM7 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
        
        in_ch     = int(64/scaleFactor)
        self.CAM8 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
        
        in_ch     = int(64/scaleFactor)
        self.CAM9 = nn.Sequential(CAM_Module(in_ch), 
                                  nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),
                                  nn.InstanceNorm2d(in_ch,affine=True),
                                  nn.LeakyReLU())
        

    def forward(self, x):
                        
        x1 = self.inc(x)
        x1 = self.CAM1(x1) # attention

        x2 = self.down1(x1)
        x2 = self.CAM2(x2) # attention

        x3 = self.down2(x2)
        x3 = self.CAM3(x3) # attention

        x4 = self.down3(x3)
        x4 = self.CAM4(x4) # attention

        x5 = self.down4(x4)
        x5 = self.CAM5(x5) # attention

        x = self.up1(x5, x4)
        x = self.CAM6(x) # attention
        
        x = self.up2(x, x3)
        x = self.CAM7(x) # attention
        
        x = self.up3(x, x2)
        x = self.CAM8(x) # attention
        
        x = self.up4(x, x1)
        x = self.CAM9(x) # attention
        
        x = self.outc(x)
        
        return x