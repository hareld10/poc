# # %matplotlib inline
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))
import torch
import torch.nn as nn
import torch.nn.functional as F


# +
# Coordinated convolution: https://arxiv.org/pdf/1807.03247.pdf
# https://github.com/Wizaron/coord-conv-pytorch/blob/master/coord_conv.py

class add_coord(nn.Module):
    
    def __init__(self,image_shape,gpus_list,batch_size):
        super(add_coord, self).__init__()
        
        image_height = image_shape[0]
        image_width  = image_shape[1]
               
#         y_coords = (2.0 * torch.arange(image_height).unsqueeze(1).expand(image_height, image_width) / (image_height - 1.0) - 1.0)# * image_height//2
        x_coords = torch.arange(image_width).unsqueeze(0).expand(image_height, image_width).float() / image_width
        y_coords = torch.arange(image_height).unsqueeze(1).expand(image_height, image_width).float() / image_height
        
        self.coords   = torch.unsqueeze(torch.stack((y_coords, x_coords), dim=0), dim=0).repeat(batch_size, 1, 1, 1)
        
        self.coords = self.coords.cuda(gpus_list[0])
        
    def forward(self,x):
        
#         print('x',x.shape)
#         print('self.coords',self.coords.shape)
        x = torch.cat((x,self.coords), dim=1)

        return x


# +
# Duel Attention: https://arxiv.org/pdf/1809.02983.pdf
# https://github.com/junfu1115/DANet/blob/master/encoding/nn/attention.py

class CAM_Module(nn.Module):
    """ Channel attention module"""
    def __init__(self, in_dim,batch_size):
        super(CAM_Module, self).__init__()
        self.chanel_in = in_dim
        self.gamma     = nn.Parameter(torch.ones(1))
        self.softmax   = nn.Softmax(dim=-1)
        
    def forward(self,x):
        """
            inputs :
                x : input feature maps( B X C X H X W)
            returns :
                out : attention value + input feature
                attention: B X C X C
        """
        m_batchsize, C, height, width = x.size()
        proj_query = x.view(m_batchsize, C, -1)
        proj_key = x.view(m_batchsize, C, -1).permute(0, 2, 1)
        energy = torch.bmm(proj_query, proj_key)
        energy_new = torch.max(energy, -1, keepdim=True)[0].expand_as(energy)-energy
        attention = self.softmax(energy_new)
        proj_value = x.view(m_batchsize, C, -1)

        out = torch.bmm(attention, proj_value)
        out = out.view(m_batchsize, C, height, width)

#         print('CAM gamma',self.gamma)
#         print('CAM out-x',self.gamma*out)

        return self.gamma*out + x


# +
class depthwise_separable_conv(nn.Module):
    def __init__(self, in_ch, out_ch, kernels_per_layer=1):
        super(depthwise_separable_conv, self).__init__()
        self.depthwise = nn.Conv2d(in_ch, in_ch * kernels_per_layer, kernel_size=3, padding=1, groups=in_ch)
        self.pointwise = nn.Conv2d(in_ch * kernels_per_layer, out_ch, kernel_size=1)

    def forward(self, x):
        out = self.depthwise(x)
        out = self.pointwise(out)
        return out
    
class depthwise_separable_conv_3d(nn.Module):
    def __init__(self, in_ch, out_ch, kernels_per_layer=1):
        super(depthwise_separable_conv_3d, self).__init__()
        self.depthwise = nn.Conv3d(in_ch, in_ch * kernels_per_layer, kernel_size=3, padding=1, groups=in_ch)
        self.pointwise = nn.Conv3d(in_ch * kernels_per_layer, out_ch, kernel_size=1)

    def forward(self, x):
        out = self.depthwise(x)
        out = self.pointwise(out)
        return out


# +
# Unet: https://github.com/milesial/Pytorch-UNet/tree/master/unet

class double_conv(nn.Module):
    def __init__(self, in_ch, out_ch,batch_size,kernel_size=3,padding=1):
        super(double_conv, self).__init__()
        self.conv1 = nn.Sequential(
            CAM_Module(in_ch,batch_size),
            nn.Conv2d(in_ch, out_ch,kernel_size=kernel_size, padding=padding),
            nn.InstanceNorm2d(out_ch,affine=True),
            nn.LeakyReLU())
        self.conv2 = nn.Sequential(
            CAM_Module(out_ch+in_ch,batch_size),
            nn.Conv2d(out_ch+in_ch, out_ch, 3, padding=1),
            nn.InstanceNorm2d(out_ch,affine=True),
            nn.LeakyReLU())

    def forward(self, x):
        x = self.conv2(torch.cat([self.conv1(x),x],dim=1))
        return x

class inconv(nn.Module):
    def __init__(self, in_ch, out_ch,batch_size):
        super(inconv, self).__init__()
        self.conv = double_conv(in_ch, out_ch,batch_size)

    def forward(self, x):
        x = self.conv(x)
        return x

class down(nn.Module):
    def __init__(self, in_ch, out_ch,batch_size):
        super(down, self).__init__()
        self.mpconv = nn.Sequential(
            nn.AvgPool2d(2),
            double_conv(in_ch, out_ch,batch_size)
        )

    def forward(self, x):
        x = self.mpconv(x)
        return x

class up(nn.Module):
    def __init__(self, in_ch, out_ch,batch_size, bilinear=False):
        super(up, self).__init__()

        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        else:
            self.up = nn.ConvTranspose2d(in_ch//2, in_ch//2, 2, stride=2)

        self.conv = double_conv(in_ch, out_ch,batch_size)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        diffX = x1.size()[2] - x2.size()[2]
        diffY = x1.size()[3] - x2.size()[3]

        if diffX<0:
            diffX = abs(diffX)
            if diffY<0:
                diffY = abs(diffY)
            x1 = F.pad(x1, (diffY // 2, diffY - diffY//2, diffX // 2, diffX + diffX//2))
        elif diffX>0:
            x2 = F.pad(x2, (diffY // 2, diffY - diffY//2, diffX // 2, diffX - diffX//2))
        elif diffX==0:# and diffY>0:
            x2 = F.pad(x2, (diffY // 2, diffY - diffY//2, diffX // 2, diffX - diffX//2))
        x = torch.cat([x2, x1], dim=1)
        x = self.conv(x)
        return x

class outconv(nn.Module):
    def __init__(self, in_ch, out_ch):
        super(outconv, self).__init__()
        self.conv = nn.Conv2d(in_ch, out_ch, 1)

    def forward(self, x):
        x = self.conv(x)
        return x

class double_conv3D(nn.Module):
    '''(conv => norm => act) * 2'''
    def __init__(self, in_ch, out_ch):
        super(double_conv3D, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv3d(in_ch, out_ch, 3, padding=1),
            nn.InstanceNorm3d(out_ch,affine=True),
            nn.LeakyReLU(),
            nn.Conv3d(out_ch, out_ch, 3, padding=1),
            nn.InstanceNorm3d(out_ch,affine=True),
            nn.LeakyReLU())

        self.rng_down =  nn.AvgPool3d(kernel_size=(2,1,1))
        self.angular_up = nn.ConvTranspose3d(out_ch, out_ch, kernel_size=(1,2,2), stride=(1,2,2))
        
    def forward(self, x):
        x = self.conv(x)
        x = self.rng_down(x)
        x = self.angular_up(x)
        return x

class conv3D_down_rng(nn.Module):
    '''(conv => norm => act) * 2'''
    def __init__(self, in_ch, out_ch):
        super(conv3D_down_rng, self).__init__()
        self.conv = nn.Sequential(
            depthwise_separable_conv_3d(in_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm3d(out_ch,affine=True),
            nn.LeakyReLU(),
            depthwise_separable_conv_3d(out_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm3d(out_ch,affine=True),
            nn.LeakyReLU())

        self.rng_down =  nn.MaxPool3d(kernel_size=(3,1,1))
        
    def forward(self, x):
        x = self.conv(x)
        x = self.rng_down(x)
        return x

class double_conv2D(nn.Module):
    '''(conv => norm => act) * 2'''
    def __init__(self, in_ch, out_ch,padding=0,kernel_size=2,stride=2,batch_size=1,output_padding=0):
        super(double_conv2D, self).__init__()
        self.conv = nn.Sequential(
            depthwise_separable_conv(in_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm2d(out_ch,affine=True),
            nn.LeakyReLU(),
            depthwise_separable_conv(in_ch, out_ch, kernels_per_layer=1),
            nn.InstanceNorm2d(out_ch,affine=True),
            nn.LeakyReLU())
        
        self.angular_up = nn.ConvTranspose2d(out_ch, out_ch, kernel_size=kernel_size, stride=stride,padding=padding,output_padding=output_padding)
        
        self.CAM = nn.Sequential(CAM_Module(out_ch,batch_size), 
                          nn.Conv2d(out_ch, out_ch, 3, padding=1, bias=True),
                          nn.InstanceNorm2d(out_ch,affine=True),
                          nn.LeakyReLU())

    def forward(self, x):
        x = self.conv(x)
        x = self.angular_up(x)
        x = self.CAM(x)
        return x


# -


class biFPN(nn.Module):
    def __init__(self,gpus_list,batch_size,x1_ch,x2_ch,x3_ch,x4_ch,x5_ch):
        super(biFPN,self).__init__()
       
        self.eps = torch.tensor(1e-4,requires_grad=False).cuda(gpus_list[0])
        
        self.w1 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w2 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w3 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w4 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w5 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w6 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w7 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w8 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w9  = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w10 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w11 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w12 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w13 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w14 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w15 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w16 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w17 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.w18 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w19 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
        self.conv1 = double_conv(x4_ch,x4_ch,batch_size,kernel_size=1,padding=0)
        self.conv2 = double_conv(x3_ch,x3_ch,batch_size,kernel_size=1,padding=0)
        self.conv3 = double_conv(x2_ch,x2_ch,batch_size,kernel_size=1,padding=0)
        self.conv4 = double_conv(x1_ch,x1_ch,batch_size,kernel_size=1,padding=0)
        self.conv5 = double_conv(x2_ch,x2_ch,batch_size,kernel_size=1,padding=0)
        self.conv6 = double_conv(x3_ch,x3_ch,batch_size,kernel_size=1,padding=0)
        self.conv7 = double_conv(x4_ch,x4_ch,batch_size,kernel_size=1,padding=0)
        self.conv8 = double_conv(x5_ch,x5_ch,batch_size,kernel_size=1,padding=0)
        
        self.downsample = nn.MaxPool2d(kernel_size=2, stride=2)
        
        self.conv_lat_1 = nn.Conv2d(x5_ch, x4_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_2 = nn.Conv2d(x4_ch, x3_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_3 = nn.Conv2d(x3_ch, x2_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_4 = nn.Conv2d(x2_ch, x1_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_5 = nn.Conv2d(x1_ch, x2_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_6 = nn.Conv2d(x2_ch, x3_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_7 = nn.Conv2d(x3_ch, x4_ch, kernel_size=1,stride=1, padding=0)
        self.conv_lat_8 = nn.Conv2d(x4_ch, x5_ch, kernel_size=1,stride=1, padding=0)
        
    def forward(self,x1,x2,x3,x4,x5):
        x4_h = self.conv1((self.w1*x4 + self.w2*F.interpolate(self.conv_lat_1(x5),x4.shape[-2:])) / (self.w1+self.w2+self.eps))
        x3_h = self.conv2((self.w3*x3 + self.w4*F.interpolate(self.conv_lat_2(x4_h),x3.shape[-2:])) / (self.w3+self.w4+self.eps))
        x2_h = self.conv3((self.w5*x2 + self.w6*F.interpolate(self.conv_lat_3(x3_h),x2.shape[-2:])) / (self.w5+self.w6+self.eps))
        
        x1_out = self.conv4((self.w7*x1 + self.w8*F.interpolate(self.conv_lat_4(x2_h),x1.shape[-2:])) / (self.w7+self.w8+self.eps))
        x2_out = self.conv5((self.w9*x2 + self.w10*x2_h + self.w11*self.downsample(self.conv_lat_5(x1_out))) / (self.w9+self.w10+self.w11+self.eps))
        x3_out = self.conv6((self.w12*x3 + self.w13*x3_h + self.w14*self.downsample(self.conv_lat_6(x2_out))) / (self.w12+self.w13+self.w14+self.eps))
        x4_out = self.conv7((self.w15*x4 + self.w16*x4_h + self.w17*self.downsample(self.conv_lat_7(x3_out))) / (self.w15+self.w16+self.w17+self.eps))
        x5_out = self.conv8((self.w18*x5 + self.w19*self.downsample(self.conv_lat_8(x4_out))) / (self.w18+self.w19+self.eps))
        
        return x1_out,x2_out,x3_out,x4_out,x5_out


class radar_encoder(nn.Module):
    def __init__(self,gpus_list,doppler_ch,batch_size):
        super(radar_encoder,self).__init__()
        
        # Radar encoder
        self.down_rng_1 = conv3D_down_rng(doppler_ch*2,64)   
        self.down_rng_2 = conv3D_down_rng(64,64)
        self.down_rng_3 = conv3D_down_rng(64,64)
        self.down_rng_4 = conv3D_down_rng(64,64) 
        self.down_rng_5 = conv3D_down_rng(64,64)

        self.conv_2d_1  = double_conv2D(64,64,batch_size=batch_size)
        self.conv_2d_2  = double_conv2D(64,64,batch_size=batch_size)
        self.conv_2d_3  = double_conv2D(64,64,batch_size=batch_size)  
        self.conv_2d_4  = double_conv2D(64,64,batch_size=batch_size)
        self.conv_2d_5  = double_conv2D(64,64,padding=(0,0),batch_size=batch_size)
#         self.conv_2d_6  = double_conv2D(64,64, padding=(52,0), kernel_size = (2,1), stride =(2,1),batch_size=batch_size)


        # Weighted residual connections
        self.eps = torch.tensor(1e-4,requires_grad=False).cuda(gpus_list[0])
        
        self.w1 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w2 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w3 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w4 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w5 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w6 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w7 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w8 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w9 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w10 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w11 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w12 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w13 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w14 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w15 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w16 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w17 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w18 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w19 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w20 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w21 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w22 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w23 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w24 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w25 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w26 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w27 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        self.w28 = nn.Parameter(torch.tensor(1,dtype=torch.float32))
        
    def forward(self,x): 
        
        # 3D encoder
        x1 = self.down_rng_1(x)
        x2 = self.down_rng_2(x1)
        x3 = self.down_rng_3((self.w1*x2+self.w2*F.interpolate(x1,x2.shape[-3:])) / (self.w1+self.w2+self.eps))
        x4 = self.down_rng_4((self.w3*x3+self.w4*F.interpolate(x2,x3.shape[-3:])+self.w5*F.interpolate(x1,x3.shape[-3:])) / (self.w3+self.w4+self.w5+self.eps))
        x5 = self.down_rng_5((self.w6*x4+self.w7*F.interpolate(x3,x4.shape[-3:])+self.w8*F.interpolate(x2,x4.shape[-3:])+self.w9*F.interpolate(x1,x4.shape[-3:]))/ (self.w6+self.w7+self.w8+self.w9+self.eps))
        
        x  = torch.squeeze(x5,2) # torch.Size([b, 128, 6, 28])
                
        # 2D super resolution
        x1 = self.conv_2d_1(x)
        x2 = self.conv_2d_2((self.w10*x1+self.w11*F.interpolate(x,x1.shape[-2:])) / (self.w10+self.w11+self.eps))
        x3 = self.conv_2d_3((self.w12*x2+self.w13*F.interpolate(x1,x2.shape[-2:])+self.w14*F.interpolate(x,x2.shape[-2:])) / (self.w12+self.w13+self.w14+self.eps))
#         print('conv_2d_3',x3.shape)
        x4 = self.conv_2d_4((self.w15*x3+self.w16*F.interpolate(x2,x3.shape[-2:])+self.w17*F.interpolate(x1,x3.shape[-2:])+self.w17*F.interpolate(x,x3.shape[-2:])) / (self.w15+self.w16+self.w16+self.w17+self.eps))
#         print('conv_2d_4',x4.shape)
        x = self.conv_2d_5((self.w18*x4+self.w19*F.interpolate(x3,x4.shape[-2:])+self.w20*F.interpolate(x2,x4.shape[-2:])+self.w21*F.interpolate(x1,x4.shape[-2:])+self.w22*F.interpolate(x,x4.shape[-2:])) /(self.w18+self.w19+self.w20+self.w21+self.w22+self.eps))
#         print('conv_2d_5',x5.shape)
#         x = self.conv_2d_6((self.w23*x5+self.w24*F.interpolate(x4,x5.shape[-2:])+self.w25*F.interpolate(x3,x5.shape[-2:])+self.w26*F.interpolate(x2,x5.shape[-2:])+self.w27*F.interpolate(x1,x5.shape[-2:])+self.w28*F.interpolate(x,x5.shape[-2:])) / (self.w23+self.w24+self.w25+self.w26+self.w27+self.w28+self.eps))
#         print('conv_2d_6',x.shape)
        return x


class task_encoder(nn.Module):
    def __init__(self,output_shape,gpus_list,batch_size,scale_factor):
        super(task_encoder,self).__init__()
        
        # Coordinated conv
        self.coord = add_coord(output_shape,gpus_list,batch_size)
        
        # Attention layers
        in_ch       = 64+2
        self.CAM_in = nn.Sequential(CAM_Module(in_ch,batch_size), nn.Conv2d(in_ch, in_ch, 3, padding=1, bias=True),nn.InstanceNorm2d(in_ch,affine=True),nn.LeakyReLU())
        
        self.inc     = inconv(64+2,                 int(64/scale_factor),batch_size=batch_size)
        self.down_1  = down(int(64/scale_factor),    int(128/scale_factor),batch_size=batch_size)
        self.down_2  = down(int(128/scale_factor),   int(256/scale_factor),batch_size=batch_size)
        self.down_3  = down(int(256/scale_factor),   int(512/scale_factor),batch_size=batch_size)
        self.down_4  = down(int(512/scale_factor),   int(512/scale_factor),batch_size=batch_size)
#         self.bifpn   = biFPN(gpus_list,batch_size,int(64/scaleFactor),int(128/scaleFactor),int(256/scaleFactor),int(512/scaleFactor),int(512/scaleFactor))

    def forward(self,x): 
        
        x1 = self.inc(self.CAM_in(self.coord(x)))
        x2 = self.down_1(x1)
        x3 = self.down_2(x2)
        x4 = self.down_3(x3)
        x5 = self.down_4(x4)
#         x1,x2,x3,x4,x5 = self.bifpn(x1,x2,x3,x4,x5) # BiFPN 2D
        
        return x1,x2,x3,x4,x5


class task_head(nn.Module):
    def __init__(self,batch_size,output_ch,scale_factor):
        super(task_head,self).__init__()
        
        self.up_1  = up(int(1024/scale_factor),    int(256/scale_factor),batch_size=batch_size)
        self.up_2  = up(int(512/scale_factor),     int(128/scale_factor),batch_size=batch_size)
        self.up_3  = up(int(256/scale_factor),     int(64/scale_factor),batch_size=batch_size)
        self.up_4  = up(int(128/scale_factor),     int(64/scale_factor),batch_size=batch_size)
        self.out_c = outconv(int(64/scale_factor), output_ch)
        
    def forward(self,x1,x2,x3,x4,x5): 
        
        x = self.up_1(x5, x4)
        x = self.up_2(x, x3)
        x = self.up_3(x, x2)
        x = self.up_4(x, x1)
        x = self.out_c(x) 
        
        return x


# +
   

class net(nn.Module):
    def __init__(self,output_shape,gpus_list,batch_size,doppler_ch,zero_pad):
        super(net, self).__init__()
                
        # Radar encoder
        self.radar_enc = radar_encoder(gpus_list,doppler_ch,batch_size)

        # Task encoder
        self.task_enc = task_encoder(output_shape,gpus_list,batch_size,scale_factor=2)
    
        # Task head
        self.task_head = task_head(batch_size,output_ch=1,scale_factor=2)

   
    def forward(self, x): # in:[b, 170*2, 256, 6, 28]    out:[b,192, 640]

#         print('in',x.shape)
        
        # Radar encoder
        x = self.radar_enc(x)
              
        # Task encoder
        x1,x2,x3,x4,x5 = self.task_enc(x)
                
        #Task decoder
        x = self.task_head(x1,x2,x3,x4,x5)
                
        return x

# +
# output_shape (254, 726)
# (127, 363)

# Transpose conv

# stride = 2
# padding = 41
# dilation = 1
# kernel_size = 2
# output_padding = 0
# input = 224
# print((input-1)*stride-2*padding+dilation*(kernel_size-1)+output_padding+1)

# #################################

#Conv

# stride = 3
# padding = 22
# dilation = 1
# kernel_size = 3
# input = 340
# print((input+2*padding-dilation*(kernel_size-1)-1)/stride+1)
