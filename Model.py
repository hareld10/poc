import numpy as np
import torch

class Model:
    def __init__(self):
        self.gpus_list = [0]
        self.model = None
        self.thresh = 0.6
        self.outputShape = None
        self.running_avg = []
        self.running_avg_len = 1
        self.camera_off = False
        self.state = None
        self.probs = None
        self.batch_size = None
        self.pretrained = True
        self.process_array = None
        self.ch_order = np.array(
            [[0, 4], [1, 5], [0, 6], [1, 7], [0, 8], [1, 9], [0, 10], [1, 11], [0, 12], [1, 13], [0, 14], [1, 15],
             [0, 16], [1, 17], [0, 18],
             [1, 19], [2, 2], [3, 3], [2, 4], [3, 5], [2, 6], [3, 7], [2, 8], [3, 9], [2, 10], [3, 11], [2, 12],
             [3, 13], [2, 14], [3, 15],
             [2, 16], [3, 17], [4, 0], [5, 1], [4, 2], [5, 3], [4, 4], [5, 5], [4, 6], [5, 7], [4, 8], [5, 9], [4, 10],
             [5, 11], [4, 12], [5, 13],
             [4, 14], [5, 15]])

        fov_az = 110  # deg
        self.min_az_fov = -fov_az // 2
        self.max_az_fov = fov_az // 2

        fov_el = 40  # deg
        self.min_el_fov = -fov_el // 2
        self.max_el_fov = fov_el // 2

        self.sant_cut_off_s = 4
        self.sant_cut_off_e = 604
        return

    def init(self):
        return

    def getRaw(self, frame):
        return

    def getIm(self, frame):
        return

    def process_S_ANT(self, S_ANT):
        S_ANT = S_ANT[self.sant_cut_off_s:self.sant_cut_off_e, :, :]

        SRe = torch.from_numpy(S_ANT.real).cuda(self.gpus_list[0])
        SIm = torch.from_numpy(S_ANT.imag).cuda(self.gpus_list[0])

        s_t = torch.zeros(SRe.shape[1], SRe.shape[0], 6, 20, 2).cuda(self.gpus_list[0])

        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 0] = SRe.permute(2, 0, 1)
        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = SIm.permute(2, 0, 1)

        ffta = torch.fft(s_t, 2, normalized=True)  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)
        array = torch.zeros(ffta.shape[0] * 2, ffta.shape[1], ffta.shape[2], ffta.shape[3]).cuda(self.gpus_list[0])

        array[::2, :, :, :] = ffta[:, :, :, :, 0]
        array[1::2, :, :, :] = ffta[:, :, :, :, 1]

        axis = 3
        array = torch.roll(array, shifts=array.shape[axis] // 2, dims=axis)
        min_idx = int(np.ceil(array.shape[axis] // 2 + self.min_az_fov / 180 * array.shape[axis]) - 1)
        max_idx = int(np.floor(array.shape[axis] // 2 + self.max_az_fov / 180 * array.shape[axis]) + 1)
        array = array[:, :, :, min_idx:max_idx]
        # array = F.interpolate(array, size=(array.shape[2], 21))
        self.process_array = array
        return self.process_array

    def apply_running_avg(self, probs):
        # Running avg
        self.running_avg.append(probs)
        if len(self.running_avg) > self.running_avg_len:
            self.running_avg.pop(0)

        probs_running_avg = torch.zeros(probs.shape).cuda(self.gpus_list[0])

        for i in range(len(self.running_avg)):
            probs_running_avg += self.running_avg[i]

        probs_running_avg /= len(self.running_avg)
        return probs_running_avg
