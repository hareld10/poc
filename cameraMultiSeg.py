from imports import *
import unet_10
from Model import Model
import os


class CameraMultiSeg(Model):
    def __init__(self, weights=None):
        super().__init__()
        self.thresh = 0.2

        self.running_avg_len = 1
        self.running_avg = []

        self.color_palette = [(255, 0, 0),  # road
                              (0, 0, 255),  # veh
                              (0, 255, 0),  # ped
                              (107, 142, 35),  # vegetation
                              # (70,130,180),    # sky
                              None,
                              # (241, 230, 255)
                              None]  # structure

        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/weights/camera_seg.v1.4.unet_10_best_model_200107.pth"
        return

    def init(self):
        self.model = unet_10.unet()
        self.model.load_state_dict(torch.load(self.weights, map_location=lambda storage, loc: storage))
        self.model = self.model.cuda(self.gpus_list[0])
        self.model.eval()
        print("Init CameraMultiSeg - Weights ", self.weights)
        return

    def getRaw(self, im):
        processed_im = torch.unsqueeze(torch.from_numpy(im).permute(2, 0, 1).float(), dim=0)
        # Get prediction
        with torch.no_grad():
            logits = torch.squeeze(self.model(Variable(processed_im, 0).cuda(self.gpus_list[0]))[0], 0)
            probs = torch.softmax(logits, axis=0)

            # probs = self.apply_running_avg(probs)

            probs_max, pred_max = torch.max(probs, axis=0)

            probs_max = self.apply_running_avg(probs_max)

            probs_max = probs_max.cpu().numpy()
            pred_max = pred_max.cpu().numpy()
            probs = probs.cpu().numpy()

        return probs, probs_max, pred_max

    def update_state(self, state):
        if "Camera" in state.thresh:
            self.thresh = state.thresh["Camera"]

        if "Camera" in state.running_avg:
            self.running_avg_len = state.running_avg["Camera"]

        return

    def getIm(self, im, imToDraw, state):

        self.probs, probs_max, pred_max = self.getRaw(im)

        pred_thresh = np.copy(pred_max)
        pred_thresh[probs_max < self.thresh] = 0

        pred_thresh = np.stack((pred_thresh, pred_thresh, pred_thresh), axis=2)
        pred_plot = np.copy(im)

        for j in range(len(self.color_palette)):
            if self.color_palette[j] is not None:
                pred_plot[np.all(pred_thresh == (j + 1, j + 1, j + 1), axis=-1)] = self.color_palette[j]

        pred_plot = cv2.addWeighted(pred_plot, 0.4, imToDraw, 0.6, 0)
        return pred_plot
