import json
from utils import non_max_suppression_fast
import pandas as pd
import glob
import cv2
import os
import numpy as np
from tqdm import tqdm
import torch
from AI_Sockets.classes import *
import matplotlib.pyplot as plt
from skimage import exposure
from radarVehNet import RadarVehNet
from radarRoadNet import RadarRoadNet
from cameraMultiSeg import CameraMultiSeg

from Manager import Manager

m = Manager()
outputShape = (int(940 // 5), int(1824 // 5))
m.init(outputShape)

# base = "/workspace/HDD/200604/200604_drive_1_W36_M8_RF1_ANT3/"
base = "/mnt/hdd/200610/200610_drive_night_W36_M8_RF1_ANT3/"
save_dir = "/mnt/hdd/videos/200610_drive_night_W36_M8_RF1_ANT3/"

base = "/mnt/hdd/200604/200604_demo_route_W36_M8_RF1_ANT3/"
save_dir = "/mnt/hdd/videos/200604_demo_route_W36_M8_RF1_ANT3/"

os.makedirs(save_dir, exist_ok=True)
df = base + "filteredData.csv"
df = pd.read_csv(df).reset_index()
# current = df.sample(100).reset_index(drop=True)
current = df[1000:]
current = current.reset_index(drop=True)

class VideoGenerator:
    def __init__(self, manager):
        self.veh_thresh = 0.3
        self.manager = manager
        self.scale_x, self.scale_y = None, None
        return

    def calculate_offline_fusion(self, rgbImg, S_ANT):
        # Object Detection
        od = base + "/camera_det_EfficientDetD7/camera_det_" + str(current.iloc[idx]["timestampCamera"]) + ".json"
        with open(od, "r") as fp:
            frame_data = json.load(fp)

        self.scale_x = rgbImg.shape[1] / outputShape[1]
        self.scale_y = rgbImg.shape[0] / outputShape[0]

        _, bb = self.manager.radar_veh_net.getIm(S_ANT, rgbImg.copy(), scale=(self.scale_x, self.scale_y))
        # fix scale
        for i in range(bb.shape[0]):
            bb[i, 0] = int((bb[i, 0]) * self.scale_x)
            bb[i, 1] = int((bb[i, 1]) * self.scale_y)
            bb[i, 2] = int((bb[i, 2]) * self.scale_x)
            bb[i, 3] = int((bb[i, 3]) * self.scale_y)

        label_bb = []
        for i in range(len(frame_data)):
            if not ((frame_data[i]['cat'] == 'car' or frame_data[i]['cat'] == 'truck' or
                     frame_data[i]['cat'] == 'motorcycle' or frame_data[i]['cat'] == 'bus') and
                    frame_data[i]['score'] > self.veh_thresh):
                continue

            x1 = float(frame_data[i]['bbox'][0])
            y1 = float(frame_data[i]['bbox'][1])
            x2 = float(frame_data[i]['bbox'][2])
            y2 = float(frame_data[i]['bbox'][3])

            label_bb.append((x1, y1, x2, y2))

        label_bb = np.array(label_bb).astype(np.float64)
        if len(label_bb) != 0 and len(bb) != 0:
            all_bb = np.concatenate((bb, label_bb), axis=0)
            final_bb = non_max_suppression_fast(all_bb)
            for i in range(final_bb.shape[0]):
                x1 = int((final_bb[i, 0]))
                y1 = int((final_bb[i, 1]))
                x2 = int((final_bb[i, 2]))
                y2 = int((final_bb[i, 3]))
                cv2.rectangle(rgbImg, (x1, y1), (x2, y2), (0, 0, 255), 2)

        # Segmentation
        seg = base + "/camera_road_seg/camera_road_seg_" + str(current.iloc[idx]["timestampCamera"]) + ".npz"
        seg = np.load(seg)["arr_0"]

        _, road_probs = self.manager.radar_road_net.getIm(S_ANT, rgbImg.copy(), scale=(self.scale_x, self.scale_y))
        _ = self.manager.camera_road.getIm(rgbImg.copy(), rgbImg.copy())

        # print("self.manager.camera_road.probs", self.manager.camera_road.probs.shape)

        c_road_probs = self.manager.camera_road.probs[1, :, :]
        radar_road_probs = road_probs[:, :, 0]

        fusion_ratio, fusion_conf = 0.5, 0.5

        probs_fusion = c_road_probs * fusion_ratio + radar_road_probs * (1 - fusion_ratio)

        # not heatmap
        pred_plot = np.copy(rgbImg)
        probs_fusion[probs_fusion>0.5] = 1
        pred_thresh = np.stack((probs_fusion,probs_fusion,probs_fusion),axis=2)
        pred_plot[np.all(pred_thresh==(1,1,1),axis=-1)] = (255, 0, 0)
        pred_plot  = cv2.addWeighted(pred_plot, 0.4, rgbImg, 0.6, 0)
        # probsImg_fusion = np.stack([probs_fusion, probs_fusion, probs_fusion], axis=2)
        # heatMap_fusion = np.uint8(exposure.rescale_intensity(np.copy(probs_fusion), out_range=(255, 0)))
        # heatMap_fusion = cv2.applyColorMap(heatMap_fusion, cv2.COLORMAP_JET)
        # heatMap_fusion = cv2.addWeighted(heatMap_fusion, 0.4, rgbImg, 0.6, 0)
        # heatMap_fusion = np.where(probsImg_fusion > fusion_conf, heatMap_fusion, rgbImg)
        return pred_plot, frame_data

    def make_video_from_dir(self, dir_path):
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        flag = False
        for i in tqdm(sorted(glob.glob(dir_path + "/*.png"), key=lambda x:int(os.path.split(x)[-1].split(".")[0]))):
            cur = cv2.imread(i)
            if not flag:
                height, width = cur.shape[:2]
                video = cv2.VideoWriter(os.path.join(dir_path, 'video3.mp4'), fourcc, 8, (width, height))
                flag = True
            video.write(cur)
        video.release()
        return



video_generator = VideoGenerator(manager=m)

for idx in tqdm(range(len(current))):
    rgbImPath = base + "/rgbCamera/rgbCamera_" + str(current.iloc[idx]["timestampCamera"]) + ".png"
    pathRadar = base + "/S_ANT/S_ANT_" + str(current.iloc[idx]["timestampRadar"]) + ".npy"

    rgbImg = cv2.imread(rgbImPath)
    S_ANT = np.load(pathRadar)

    state = AIState()

    # state.screens = ["Radar Road Vehicle", "Camera", "Fusion", "Range Doppler"]
    state.screens = ["Radar Perception", "Camera", "Fusion", "Range Doppler"]
    state.camera_off = False
    state.od_visualization = 1

    state.thresh = {}
    state.thresh["Camera Road"] = 0.6
    state.thresh["Radar Road"] = 0.6
    state.thresh["Radar Vehicle"] = 0.6

    state.running_avg = {"Radar": 1, "Camera":1}

    m.state = state
    m.update_state(state)
    fusion_frame, frame_data = video_generator.calculate_offline_fusion(rgbImg.copy(), S_ANT)
    m.fusion_frame = fusion_frame
    m.frame_data = np.array(frame_data)

    im = m.getIm(state, S_ANT, rgbImg)
    cv2.imwrite(save_dir + "/" + str(current.iloc[idx]["timestampCamera"]) + ".png", im)
    # plt.figure(figsize=(20,10))
    # plt.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
    # plt.savefig("./images/" + str(idx) + ".png")
    # plt.show()


video_generator.make_video_from_dir(save_dir)
