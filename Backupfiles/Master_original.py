import sys
import os
import concurrent.futures
from queue import *

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + "/weights/")
sys.path.insert(0, dir_path + "/models/")

from radarRoad import RadarRoad
from cameraRoad import CameraRoad
import numpy as np
import cv2
from skimage import exposure 
import time
import concurrent.futures
from Manager import Manager
from concurrent.futures import ThreadPoolExecutor

class Master:
    def __init__(self):
        self.m1 = None
        self.m2 = None
        self.m3 = None
        self.executor = ThreadPoolExecutor(max_workers=4)
        self.turn = 1
        self.q_in = Queue(maxsize=3)
        self.q_out = Queue(maxsize=3)
        self.executor.submit(self.inputLoop)
        
    def init(self, outputShape):
        self.m1 = Manager()
        self.m2 = Manager()
        self.m3 = Manager()
        
        self.m1.init(outputShape)
        self.m2.init(outputShape)
        self.m3.init(outputShape)
    
    def setIm(self, rd, rgbImg, radarRoadThresh, cameraRoadThresh):
        if self.q_in.full():
            return   
        if self.turn == 1:
            # Submit to manager1
            print("Master: Submitting to 1")
            self.q_in.put(self.executor.submit(self.m1.getIm, rd, rgbImg, radarRoadThresh, cameraRoadThresh), block=False)
            self.turn = 2
        elif self.turn == 2:
            # Submit to manager2
            print("Master: Submitting to 2")
            self.q_in.put(self.executor.submit(self.m2.getIm, rd, rgbImg, radarRoadThresh, cameraRoadThresh), block=False)
            self.turn = 3
        else:
            # Submit to manager3
            print("Master: Submitting to 3")
            self.q_in.put(self.executor.submit(self.m3.getIm, rd, rgbImg, radarRoadThresh, cameraRoadThresh), block=False)
            self.turn = 1
            
    def inputLoop(self):
        while True:
            if not self.q_in.empty():
                #print("Master: geting result from q_in")
                ob = self.q_in.get(block=False).result()
                #print("Master: Got object to q_out")
#                 if not self.q_out.full():
                self.q_out.put(ob, block=True)
                #print("Master: After Putting to q")
            time.sleep(0.001)
                
    
    def getIm(self):
        if not self.q_out.empty():
            return self.q_out.get(block=True)
        else:
            return None
    
#     def getIm(self, rd, rgbImg, radarRoadThresh, cameraRoadThresh):
#         if self.turn == 1:
#             # Submit to manager1
#             cur = executor.submit(self.m1.getIm, rd, rgbImg, radarRoadThresh, cameraRoadThresh)
#             self.q.put(cur)
#             self.turn = 2
#         else:
#             # Submit to manager2
#             cur = executor.submit(self.m2.getIm, rd, rgbImg, radarRoadThresh, cameraRoadThresh)
#             self.q.put(cur)
#             self.turn = 1
            
#         while not q.empty():
#             return q.get().result()
    
    
    
    
    