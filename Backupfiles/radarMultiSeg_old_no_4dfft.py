from imports import *
import net_radar_seg_v5
from Model import Model
from radarRoad import RadarRoad

class RadarMultiSeg(RadarRoad):
    def __init__(self, road=True, vehicle=False, weights=None):
        super().__init__()
        self.road_vehicle_dict = {"weights": "/weights/radar_road_vehicle_segmentation.v1.2.net_radar_seg_v5_best_model_191231.pth"}
        self.color_palette = [(255,0,0), # road
                                (0,0,255)] # veh)

        self.thresh_vehicle = 0.65
        self.thresh_road = 0.65

        self.mode = "road_vehicle"
        if road and vehicle:
            self.mode = "road_vehicle"
            self.mode_dict = self.road_vehicle_dict
        else:
            print("In RadarMultiSeg: Please define at least 2 classes")

        if weights:
            self.weights = weights
        else:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]


    def init(self, outputShape, batch_size=1):

        self.outputShape = outputShape
        self.batch_size = batch_size

        self.model = net_radar_seg_v5.net(self.outputShape, self.gpus_list, 1)  

        self.model_init()

    def getRaw(self, S_ANT):
        s = self.process_S_ANT(S_ANT)
        with torch.no_grad():
            modelInput =  torch.unsqueeze(Variable(torch.from_numpy(s)).cuda(self.gpus_list[0]),0)
            logits     = torch.squeeze(self.model(modelInput),0)
            probs      = torch.softmax(logits,axis = 0)
            probs_max,pred_max = torch.max(probs,axis=0)
            return probs_max.cpu().numpy(), pred_max.cpu().numpy()


    def getIm(self, array, imToDraw):
        probs_max, pred_max = self.getRaw(array)

        #pred_max[probs_max<self.thresh] = 0
        pred_max = np.stack((pred_max,pred_max,pred_max),axis=2)
        

        #print("addWeighted ", imToDraw.shape, pred_plot.shape)
        if self.camera_bg:
            pred_plot = np.copy(imToDraw)
            #pred_plot[np.all(pred_max==(1,1,1),axis=-1)] = self.color_palette[0] # road
            # pred_plot[np.all(pred_max==(2,2,2),axis=-1)] = self.color_palette[1] # vehicle

            pred_plot[np.all(np.logical_and(pred_max==(1,1,1),axis=-1), probs_max>=self.thresh_road)] = self.color_palette[0] # road
            pred_plot[np.all(np.logical_and(pred_max==(2,2,2),axis=-1), probs_max>=self.thresh_vehicle)] = self.color_palette[1] # vehicle
            pred_plot  = cv2.addWeighted(pred_plot, 0.4, imToDraw, 0.6, 0)
        else:
            pred_plot = np.zeros(imToDraw.shape, dtype=imToDraw.dtype)
            pred_plot[np.all(pred_max==(1,1,1),axis=-1)] = self.color_palette[0] # road
            pred_plot[np.all(pred_max==(2,2,2),axis=-1)] = self.color_palette[1] # vehicle
        return pred_plot
        
