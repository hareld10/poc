import sys
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + "/weights/")
sys.path.insert(0, dir_path + "/models/")

from radarRoad import RadarRoad
from cameraRoad import CameraRoad
from radarMultiSeg import RadarMultiSeg
from cameraMultiSeg import CameraMultiSeg

import numpy as np
import cv2
from skimage import exposure 
import time
import concurrent.futures
import torch

class Manager:
    def __init__(self):
        self.radar_road = None
        self.camera_road = None
        self.radar_road_vehicle = None
        self.outputShape = None
        self.state = None
        self.imph, self.impw, self.c = 0, 0, 3
        self.imh, self.imw = 0, 0

        self.downPass = 35
        self.padFromAbove = 80
        self.padBetween = 20

        self.pad_from_side_4_5 = 50
        self.pad_between_4_5 = 100
        self.padFromDownRadar = 60

        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.color = (243, 150, 33)

        self.color_palette = [(255,0,0),       # road
                             (0,0,255),       # veh
                             (0,255,0),       # ped
                             (107,142, 35),   # vegetation
                             (70,130,180),    # sky
                             (241, 230, 255)] # structure
        self.camera_off = False

        # Fusion
        self.p_r, self.p_c, self.rd, self.finalIm, self.rgbImg  = None, None, None, None, None
        self.screens = None
        self.titles = {"Radar Road":  "Radar Perception", "Radar Road Vehicle":  "Radar Perception", "Camera":"Camera Perception", "Fusion":"Sensor Fusion",
                        "Range Doppler":"Range-Doppler",
                        "Radar Road Heatmap": "Radar Road Heatmap",
                        "Radar Vehicle Heatmap": "Radar Vehicle Heatmap",
                        "Radar Combine Heatmap": "Radar Heatmap"}

        self.scale_screen_0 = 2.8
        self.scale_screen_6_7 = 1.5
        self.imh_6, self.imw_6 = None, None
        return
    
    def init(self, outputShape):
        self.camera_road = CameraMultiSeg()
        self.radar_road_vehicle = RadarMultiSeg(road=True, vehicle=True)

        self.outputShape = outputShape
        self.camera_road.init(outputShape)
        self.radar_road_vehicle.init(outputShape)
        self.imw, self.imh = self.outputShape

        return True
    
    def getFusionMulti(self, p1, p2, imToDraw):
        if p1 is None or p2 is None:
            print("Manager-getFusionMulti: Some is None")
            return

        fusion_ratio, fusion_conf = 0.6, 0.6

        # print("self.p1 ", p1.shape)
        # print("self.p2 ", p2.shape)
        # print("p1 ", p1[:, 0, 0])
        # print("p2 ", p2[:, 0, 0])

        probs_fusion    = p1*fusion_ratio + p2*(1-fusion_ratio)

        # probs_max,pred_max = torch.max(torch.from_numpy(probs_fusion),axis=0)

        # probs_max = probs_max.cpu().numpy()
        # pred_max = pred_max.cpu().numpy()

        probs_max = np.max(probs_fusion,axis=0)
        pred_max = np.argmax(probs_fusion,axis=0)

        pred_thresh = np.copy(pred_max)
        #pred_thresh[probs_max<0.6] = 0
        
        pred_thresh  = np.stack((pred_thresh,pred_thresh,pred_thresh),axis=2)
        pred_plot = np.copy(imToDraw)
        
        for j in range(len(self.color_palette)):
            pred_plot[np.all(pred_thresh==(j+1,j+1,j+1),axis=-1)] = self.color_palette[j] 

        pred_plot  = cv2.addWeighted(pred_plot, 0.4, imToDraw, 0.6, 0)
        return pred_plot

    def getFusion(self, probs_running_avg, c_probs_running_avg, rgbImg):
        if probs_running_avg is None or c_probs_running_avg is None:
            return
        fusion_ratio, fusion_conf = 0.6, 0.6
        probs_fusion    = probs_running_avg*fusion_ratio + c_probs_running_avg*(1-fusion_ratio)
        probsImg_fusion = np.stack([probs_fusion,probs_fusion,probs_fusion],axis=2)
        heatMap_fusion  = np.uint8(exposure.rescale_intensity(np.copy(probs_fusion), out_range=(255, 0)))
        heatMap_fusion  = cv2.applyColorMap(heatMap_fusion, cv2.COLORMAP_JET)        
        heatMap_fusion  = cv2.addWeighted(heatMap_fusion, 0.4, rgbImg, 0.6, 0)
        heatMap_fusion  = np.where(probsImg_fusion>fusion_conf,heatMap_fusion,rgbImg)
        return heatMap_fusion
    
    def scale(self, x, out_range=(0, 255), axis=None):
        domain = np.min(x, axis), np.max(x, axis)
        y = (x - (domain[1] + domain[0]) / 2) / (domain[1] - domain[0])
        return y * (out_range[1] - out_range[0]) + (out_range[1] + out_range[0]) / 2
    
    def getRD(self, rd):
        z = np.sum(np.abs(rd), axis=1)
        z = np.fft.fftshift(z, 1).T
        rdz = 20*np.log10(z)
        rdz  = np.uint8(exposure.rescale_intensity(np.copy(rdz), out_range=(0, 255)))
        rdz  = cv2.applyColorMap(rdz, cv2.COLORMAP_JET)
        rdz = cv2.resize(rdz, self.outputShape)
        return rdz
    
    def set_screen_1(self, padded, im, text):
        imh, imw, c = im.shape
        padded[-imh-self.downPass:-self.downPass, :imw , :] = im
        
        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((imw - textsize[0]) / 2)
        textY = int(self.imph - imh - textsize[1] -self.downPass )
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_0(self, padded, im, text):
        im = cv2.resize(im,None,fx=self.scale_screen_0,fy=self.scale_screen_0)

        imrh, imrw, c = im.shape
        padFromTheSide = int(self.impw / 2 - imrw / 2)
        
        padded[self.padFromAbove:self.padFromAbove + imrh, padFromTheSide: padFromTheSide + imrw , :] = im

        fontScale = 2
        thickness = 5
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((padded.shape[1] - textsize[0]) / 2)
        textY = int((70 + textsize[1]) / 2)
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_2(self, padded, im, text):
        padded[-self.imh-self.downPass:-self.downPass, self.imw + self.padBetween: self.imw + self.padBetween + self.imw, :] = im
        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((self.imw + self.padBetween) + (self.imw - textsize[0]) / 2)
        textY = int(self.imph - self.imh - textsize[1] -self.downPass)
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_3(self, padded, im, text):
        rdh, rdw, c = im.shape
        padded[-rdh-self.downPass:-self.downPass, 2*self.imw + 2*self.padBetween: 2*self.imw + 2*self.padBetween + rdw, :] = im
        
        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((2*self.imw + 2*self.padBetween) + (self.imw - textsize[0]) / 2)
        textY = int(self.imph - self.imh - textsize[1] -self.downPass )
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_4(self, padded, im, text):
        imh, imw, c = im.shape
        padded[-imh-self.downPass:-self.downPass, :imw , :] = im
        
        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]

        textX = int((imw - textsize[0]) / 2)
        textY = int(self.imph - self.imh - textsize[1] -self.downPass )
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_5(self, padded, im, text):
        imh, imw, c = im.shape
        padded[-imh-self.downPass:-self.downPass, self.imw + self.padBetween: self.imw + self.padBetween + imw, :] = im
        
        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]

        textX = int((self.imw + self.padBetween) + (self.imw - textsize[0]) / 2)

        textY = int(self.imph - self.imh - textsize[1] -self.downPass )
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_6(self, padded, im, text):
        im = cv2.resize(im,None,fx=self.scale_screen_6_7 ,fy=self.scale_screen_6_7)

        self.imh_6, self.imw_6, c = im.shape        

        # screen 1
        padded[self.padFromAbove:self.padFromAbove + self.imh_6, :self.imw_6 , :] = im

        fontScale = 2
        thickness = 5
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((self.imw_6 - textsize[0]) / 2)
        textY = int((70 + textsize[1]) / 2)
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_7(self, padded, im, text):
        im = cv2.resize(im,None,fx=self.scale_screen_6_7,fy=self.scale_screen_6_7)

        imh, imw, c = im.shape
        
        # SCreen2
        padded[self.padFromAbove:self.padFromAbove + imh, self.imw_6 + self.padBetween: self.imw_6 + self.padBetween + imw, :] = im

        fontScale = 2
        thickness = 5
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((self.imw_6  + self.padBetween) + (imw - textsize[0]) / 2)
        textY = int((70 + textsize[1]) / 2)
        cv2.putText(padded, text, (textX, textY ), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def dummy(self, empty_arg):
        return

    def update_state(self, state):
        # set Camera BackGround
        self.radar_road_vehicle.camera_off = state.camera_off
        self.camera_off = state.camera_off

        if "Camera" in state.thresh:
            self.camera_road.thresh = state.thresh["Camera"]
        if "Radar Road" in state.thresh:
            self.radar_road_vehicle.thresh_road = state.thresh["Radar Road"]
        if "Radar Vehicle" in state.thresh:
            self.radar_road_vehicle.thresh_vehicle = state.thresh["Radar Vehicle"]

        # Running Avg (Need to set to multi Seg)
        if "Radar" in state.running_avg:
            self.radar_road_vehicle.running_avg_len = state.running_avg["Radar"]

        if "Camera" in state.running_avg:
            self.camera_road.running_avg_len = state.running_avg["Camera"]

        self.callers = {"Radar Road Vehicle":   (self.radar_road_vehicle.getIm, self.rd, self.finalIm),
                        "Camera":          (self.camera_road.getIm, self.rgbImg, self.finalIm),
                        "Fusion":               (self.dummy, None),
                        "Range Doppler":        (self.getRD, self.rd)
                        }
                        
        return

    def get_mode(self, screens):
        if None in screens:
            return 3
        return 4

    def getIm(self, state, rd, rgbImg):
        print("Manager ", os.getpid(), " getIm")
        fps_time = time.time() # For FPS

        # shape = rgbImg.shape
        # return np.random.permutation(rgbImg.flatten()).reshape(shape)


        self.p_c = None
        self.p_r = None

        self.rgbImg = cv2.resize(rgbImg, self.outputShape)
        self.finalIm = np.copy(self.rgbImg)
        self.rd = rd

        # Default Screens
        self.screens = {0: (self.radar_road_vehicle.getIm, self.rd, self.finalIm)}
        self.update_state(state)
        
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:

            if self.camera_off:
                for index, k in enumerate(state.screens):
                    if "Camera" == k:
                        state.screens[index] = None


            desired_inputs = set(state.screens)
            # Process Radar Always
            desired_inputs.add("Radar Road Vehicle")

            self.screens_input = {}
            self.screens_output = {}
            cur_dtype = None

            for input_name in desired_inputs:
                if input_name is not None and input_name in self.callers: 
                    self.screens_input[input_name] = executor.submit(*self.callers[input_name])        

            if "Radar Road Vehicle" in desired_inputs:
            	# self.screens_output["Radar Road Vehicle"] = self.rgbImg

                self.screens_output["Radar Road Vehicle"] = self.screens_input["Radar Road Vehicle"].result()
                #self.p_r = self.radar_road_vehicle.probs
                cur_dtype = self.screens_output["Radar Road Vehicle"].dtype

                # if self.camera_off:
                #     self.screens_output["Radar Road Heatmap"] = self.radar_road_vehicle.get_heatmap_im(0)
                #     self.screens_output["Radar Vehicle Heatmap"] = self.radar_road_vehicle.get_heatmap_im(1)
                
            if "Camera" in desired_inputs:
                cameraOut = self.screens_input["Camera"].result()
                #self.p_c = self.camera_road.probs
                cur_dtype = cameraOut.dtype
                self.screens_output["Camera"] = cameraOut
            else:
                self.p_c = None


            if "Fusion" in desired_inputs and self.p_r is not None and self.p_c is not None:
            	self.screens_output["Fusion"] = self.rgbImg
                # self.screens_input["Fusion"] = executor.submit(self.getFusionMulti, self.p_r, self.p_c[:3, :, :], self.rgbImg)
                # self.screens_output["Fusion"] = self.screens_input["Fusion"].result()

            elif self.camera_off and "Radar Road Vehicle" in desired_inputs:
                 self.screens_output["Fusion"] = self.screens_output["Radar Road Vehicle"]
            else:
                self.screens_output["Fusion"] = self.rgbImg 

            if "Range Doppler" in desired_inputs:
                self.screens_output["Range Doppler"] = self.screens_input["Range Doppler"].result()
                cur_dtype = self.screens_output["Range Doppler"].dtype
            else:
                self.screens_output["Range Doppler"] = self.rgbImg

            if "Radar Combine Heatmap" in desired_inputs and "Radar Road Vehicle" in desired_inputs:
                self.screens_output["Radar Combine Heatmap"] = self.radar_road_vehicle.get_heatmap_im(2)
            

            mode = self.get_mode(state.screens)
            # Setting Camera Off:
            if self.camera_off:
                padded = np.zeros((int(self.imh + self.imh*self.scale_screen_6_7 + 120 + self.downPass + self.padFromDownRadar), int(self.imw*3 + 2*self.padBetween), self.c), dtype=cur_dtype)
                self.imph, self.impw, self.c = padded.shape

                state.screens[0] = "Radar Road Heatmap"
                state.screens[1] = "Radar Vehicle Heatmap"
                state.screens[2] = "Range Doppler"

                padded = self.set_screen_6(padded, self.screens_output[state.screens[0]], self.titles[state.screens[0]])
                padded = self.set_screen_7(padded, self.screens_output[state.screens[1]], self.titles[state.screens[1]])
                padded = self.set_screen_2(padded, self.screens_output[state.screens[2]], self.titles[state.screens[2]])


            elif mode == 4:
                padded = np.zeros((int(self.imh + self.imh*self.scale_screen_0 + 120 + self.downPass + self.padFromDownRadar), int(self.imw*3 + 2*self.padBetween), self.c), dtype=cur_dtype)
                self.imph, self.impw, self.c = padded.shape

                if state.screens[1] is not None:
                    cur_name = state.screens[1]
                    padded = self.set_screen_1(padded, self.screens_output[cur_name], self.titles[cur_name])

                if state.screens[2] is not None:
                    cur_name = state.screens[2]
                    padded = self.set_screen_2(padded, self.screens_output[cur_name], self.titles[cur_name])

                if state.screens[3] is not None:
                    cur_name = state.screens[3]
                    padded = self.set_screen_3(padded, self.screens_output[cur_name], self.titles[cur_name])

                if state.screens[0] is not None:
                    cur_name = state.screens[0]
                    padded = self.set_screen_0(padded, self.screens_output[cur_name], self.titles[cur_name])
            
            elif mode == 3:
                padded = np.zeros((int(self.imh + self.imh*self.scale_screen_0 + 120 + self.downPass + self.padFromDownRadar), int(self.imw*2 + 1*self.padBetween), self.c), dtype=cur_dtype)
                self.imph, self.impw, self.c = padded.shape

                part = [index for index, i in enumerate(state.screens) if i is not None]

                if state.screens[0] is not None and len(part) > 0:
                    print("Screens ", state.screens)
                    cur_name = state.screens[part[0]]
                    print("self.screens_output[cur_name].shape ", self.screens_output[cur_name], cur_name)
                    padded = self.set_screen_0(padded, self.screens_output[cur_name], self.titles[cur_name])

                if state.screens[part[1]]is not None and len(part) > 1 :
                    cur_name = state.screens[part[1]]
                    padded = self.set_screen_4(padded, self.screens_output[cur_name], self.titles[cur_name])

                if state.screens[part[2]] is not None and len(part) > 2:
                    cur_name = state.screens[part[2]]
                    padded = self.set_screen_5(padded, self.screens_output[cur_name], self.titles[cur_name])

            
            
            return padded
