#!/bin/bash

#1. Mount disks
echo -e "\nTrying to find disk...\n"

dev=`sudo fdisk -l | grep filesystem | grep /dev/sd | cut -d " " -f1`
echo -e "$dev"

if [ -z "$dev" ] ; then

echo -e “\n\n$(date) NO SSD Found!!!\n\n”

else

echo -e "\nFound $dev, please type password to mount\n"
sudo umount $dev
sudo mkdir -p /media/nvidia/wiseData
sudo mount -t ext4 $dev /media/nvidia/wiseData
echo -e "\n$dev is mounted to /media/nvidia/wiseData\n"

fi

# 1. run doker

# CONT=$(sudo docker ps -q | head -n 1)
# echo $CONT
# docker exec -it $CONT python3 ../POC/runDemo.py &

#2. run local systemManager
cd $HOME/workspace/wise_nvidia_system/System/;
./SystemManager &

#3. run ptp as master
sudo ptp4l -f /etc/linuxptp/ptp4l.conf -i eth0 -m -S > $HOME/workspace/wise_sdk/ptp.txt &

#4. run GUI
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/workspace/vtk/install/lib/
export PYTHONPATH=${PYTHONPATH}:${HOME}/workspace/vtk/install/lib/python3.6/site-packages

cd $HOME/workspace/wise_sdk/SDK/python; 
python3 CarDemoGUI.py

pkill -9 SystemManager
pkill -9 DataController
pkill -9 RFCoontroller
