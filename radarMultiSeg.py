from imports import *
import net_radar_seg_v8
from Model import Model
from radarRoad import RadarRoad
import time
import gc
import matplotlib.colors as colors
import matplotlib as mpl
from matplotlib import cm

class RadarMultiSeg(RadarRoad):
    def __init__(self, road=True, vehicle=True, weights=None):
        super().__init__()

        self.road_vehicle_dict = {"weights": "/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_best_model_200615.pth"}
        # self.road_vehicle_dict = {
            # "weights": "/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_epoch_4_200615.pth"}
        # self.road_vehicle_dict = {
            # "weights": "/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_200612_epoch_5.pth"}
        # self.road_vehicle_dict = {
        #     "weights": "/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_best_model_200614.pth"}

        self.color_palette = [(255, 0, 0),  # road
                              (0, 0, 255),  # veh
                              (0, 255, 0),  # ped
                              (107, 142, 35),  # vegetation
                              (70, 130, 180),  # sky
                              (241, 230, 255)]  # structure

        self.thresh_vehicle = 0.71
        self.thresh_road = 0.56
        self.state = None

        self.peak_thresh_veh = 0.6
        self.nms_thresh_veh = 0.2
        self.bb_thresh_veh = 0.25

        self.fov_az_camera = 59.5
        self.fov_el_camera = 29

        self.fontsize = 25
        self.fontScale = 0.9
        self.thickness = 2

        self.thresh_heatmap = 0.2
        self.heatmaps_im = [None] * 6

        self.colormap = cm.ScalarMappable(norm=mpl.colors.Normalize(vmin=0, vmax=300), cmap=cm.autumn_r)

        self.ch_order = np.array(
            [[0, 4], [1, 5], [0, 6], [1, 7], [0, 8], [1, 9], [0, 10], [1, 11], [0, 12], [1, 13], [0, 14], [1, 15],
             [0, 16], [1, 17], [0, 18],
             [1, 19], [2, 2], [3, 3], [2, 4], [3, 5], [2, 6], [3, 7], [2, 8], [3, 9], [2, 10], [3, 11], [2, 12],
             [3, 13], [2, 14], [3, 15],
             [2, 16], [3, 17], [4, 0], [5, 1], [4, 2], [5, 3], [4, 4], [5, 5], [4, 6], [5, 7], [4, 8], [5, 9], [4, 10],
             [5, 11], [4, 12], [5, 13],
             [4, 14], [5, 15]])

        self.mode = "road_vehicle"
        if road and vehicle:
            self.mode = "road_vehicle"
            self.mode_dict = self.road_vehicle_dict
        else:
            print("In RadarMultiSeg: Please define at least 2 classes")

        d_path = os.path.dirname(os.path.realpath(__file__))
        if weights:
            self.weights = d_path + "/" + weights
        else:
            self.weights = d_path + "/" + self.mode_dict["weights"]

    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size

        self.model = net_radar_seg_v8.net(self.outputShape, self.gpus_list, 1)

        self.model_init()

    def process_S_ANT(self, S_ANT):
        S_ANT = S_ANT[:600, :, :]

        SRe = torch.from_numpy(S_ANT.real).cuda(self.gpus_list[0])
        SIm = torch.from_numpy(S_ANT.imag).cuda(self.gpus_list[0])

        s_t = torch.zeros(SRe.shape[1], SRe.shape[0], 6, 20, 2).cuda(self.gpus_list[0])

        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 0] = SRe.permute(2, 0, 1)
        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = SIm.permute(2, 0, 1)

        ffta = torch.fft(s_t, 2, normalized=True)  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)
        array = torch.zeros(ffta.shape[0] * 2, ffta.shape[1], ffta.shape[2], ffta.shape[3]).cuda(self.gpus_list[0])

        array[::2, :, :, :] = ffta[:, :, :, :, 0]
        array[1::2, :, :, :] = ffta[:, :, :, :, 1]

        array = F.interpolate(array, size=(array.shape[2], 21))
        self.process_array = array
        return array

    def getRaw(self, S_ANT):
        s = self.process_S_ANT(S_ANT)

        with torch.no_grad():
            modelInput = torch.unsqueeze(Variable(s), 0)
            logits = torch.squeeze(self.model(modelInput), 0)
            probs = torch.softmax(logits, axis=0)

            probs_max, pred_max = torch.max(probs, axis=0)

            probs_max = probs_max.cpu().numpy()
            pred_max = pred_max.cpu().numpy()
            probs = probs.cpu().numpy()

            return probs, probs_max, pred_max

    def get_heatmap_im(self, index):
        if len(self.heatmaps_im) > index:
            return self.heatmaps_im[index]
        else:
            print("Radar Multi Seg - Heatmaps, Warning Index out of range")
        return None

    def gen_heatmap(self, probs, imToDraw, thresh, color_map=cv2.COLORMAP_JET, rev=False):
        probsImg = np.stack([probs, probs, probs], axis=2)
        if not rev:
            heatMap = np.uint8(exposure.rescale_intensity(np.copy(probs), out_range=(255, 0)))
        else:
            heatMap = np.uint8(exposure.rescale_intensity(np.copy(probs), out_range=(0, 255)))
        heatMap = cv2.applyColorMap(heatMap, color_map)
        heatMap = np.where(probsImg > thresh, heatMap, imToDraw)
        return heatMap

    def update_state(self, state):
        if "Radar Road" in state.thresh:
            self.thresh_road = float(state.thresh["Radar Road"])

        if "Radar Vehicle" in state.thresh:
            self.thresh_vehicle = float(state.thresh["Radar Vehicle"])

        if "Veh Peak" in state.thresh:
            self.peak_thresh_veh = float(state.thresh["Veh Peak"])

        if "Veh NMS" in state.thresh:
            self.nms_thresh_veh = float(state.thresh["Veh NMS"])

        return

    def getIm(self, array, imToDraw, state, seg_instance=False, road_probs=None):
        self.update_state(state)
        self.heatmaps_im = [None] * 3
        self.probs, probs_max, pred_max = self.getRaw(array)

        if road_probs is not None:
            self.probs[1, :, :] = road_probs

        # Gen HeatMaps
        # self.heatmaps_im[0] = self.gen_heatmap(self.probs[1, :, :], np.zeros(imToDraw.shape, dtype=imToDraw.dtype), thresh=self.thresh_road, color_map=cv2.COLORMAP_WINTER)
        # self.heatmaps_im[1] = self.gen_heatmap(self.probs[2, :, :], np.zeros(imToDraw.shape, dtype=imToDraw.dtype), thresh=self.thresh_vehicle, color_map=cv2.COLORMAP_AUTUMN)
        # self.heatmaps_im[2] = cv2.addWeighted(self.heatmaps_im[0], 1, self.heatmaps_im[1], 1, 0)
        # idx=np.all(self.heatmaps_im[2]==(0,0,0), axis=-1)
        # self.heatmaps_im[2][idx]=imToDraw[idx]
        # self.heatmaps_im[2] = cv2.addWeighted(self.heatmaps_im[2], 0.4, imToDraw, 0.6, 0)


        pred_max = np.stack((pred_max, pred_max, pred_max,), axis=2)
        
        # pred_plot[np.logical_and(np.all(pred_max == (1, 1, 1), axis=-1), probs_max >= self.thresh_road)] = self.color_palette[0]  # road

        road_probs = cv2.resize(self.probs[1, :, :], (imToDraw.shape[1], imToDraw.shape[0]))
        if not state.camera_off:
            pred_plot = self.gen_heatmap(road_probs, imToDraw, thresh=self.thresh_road, color_map=cv2.COLORMAP_SUMMER)
        else:
            pred_plot = self.gen_heatmap(road_probs, np.zeros(imToDraw.shape, dtype=imToDraw.dtype), thresh=self.thresh_road, color_map=cv2.COLORMAP_SUMMER)

        if seg_instance:
            if not state.camera_off:
                pred_plot = cv2.addWeighted(pred_plot, 0.4, imToDraw, 0.6, 0)
            probs_vehicle = self.probs[2, :, :]
            centers = np.stack([probs_vehicle, probs_vehicle, probs_vehicle], axis=2)
            peaks = detect_peaks(centers, self.peak_thresh_veh)
            peaks = peaks[:, :, 0] * 1
            row, col = peaks.nonzero()
            bb = box_nms_heatmap(row, col, probs_vehicle, self.nms_thresh_veh, self.bb_thresh_veh)

            # Sort bb
            # bb = sort_bb(bb)

            # Get radar data
            img_width = pred_plot.shape[1]
            img_height = pred_plot.shape[0]

            img_scale_x = pred_plot.shape[1] / self.outputShape[0]
            img_scale_y = pred_plot.shape[0] / self.outputShape[1]

            pred_max_full_res = cv2.resize(pred_max.astype(np.uint8),
                                           (pred_plot.shape[1], pred_plot.shape[0]))

            for i in range(bb.shape[0]):
                x1 = int(bb[i, 0] * img_scale_x)
                y1 = int(bb[i, 1] * img_scale_y)
                x2 = int(bb[i, 2] * img_scale_x)
                y2 = int(bb[i, 3] * img_scale_y)
                score = "%.2f" % bb[i, 4]
                cx = (x2 - x1) // 2 + x1
                cy = (y2 - y1) // 2 + y1

                az = np.round((cx - img_width/2) / img_width * self.fov_az_camera, 1)
                el = np.round((img_height/2 - cy) / img_height * self.fov_el_camera, 1)

                # Calc BB Color and mask with seg
                inside_bb = np.zeros((pred_plot.shape[0], pred_plot.shape[1]), dtype=pred_plot.dtype)
                inside_bb[y1:y2, x1:x2] = True
                condition = np.logical_and(np.all(pred_max_full_res == (2, 2, 2), axis=-1), inside_bb)
                full_mask = np.copy(pred_plot)

                # Get Color defined by range
                dist = np.sqrt(np.abs((x2-x1)*(y2-y1)))
                cur_color = self.colormap.to_rgba(dist)[:3]
                cur_color = np.multiply(cur_color, 255)
                cur_color = cur_color[::-1]
                full_mask[condition] = cur_color
                pred_plot = cv2.addWeighted(full_mask, 0.4, pred_plot, 0.6, 0)

                # Plot Second Row And Gray Box
                #         sec_row =  'R:'+str(rng) + " A:" +str(az)
                sec_row = 'Az:' + str(az) + ' E:' + str(el)

                (label_width, label_height), baseline = cv2.getTextSize(sec_row, fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                                                        fontScale=self.fontScale, thickness=self.thickness)
                label_height = int(label_height)
                label_width = int(label_width * 1.05)

                # Gray Box with ref to sec-row width
                rec_height = int(label_height * 3.5 + 35)
                pred_plot[y1 - rec_height:y1 - 5, x1:x1 + label_width] = pred_plot[y1 - rec_height:y1 - 5,
                                                                   x1:x1 + label_width] * 0.5

                cx = x1 - 5
                cy = y1 - label_height * 2 - 10
                cv2.putText(pred_plot, sec_row, (cx, cy), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=self.fontScale,
                            color=(0, 255, 0), lineType=1, thickness=self.thickness)

                # Plot First Row
                first_row = "Vehicle score: " + str(score)
                (label_width, label_height), baseline = cv2.getTextSize(first_row, fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                                                        fontScale=self.fontScale, thickness=self.thickness)
                cx = x1 - 5
                cy = y1 - label_height * 3 - 20
                cv2.putText(pred_plot, first_row, (cx, cy), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=self.fontScale,
                            color=(0, 255, 0), lineType=1, thickness=self.thickness)

                # Plot Third Row
                #         third_row =  'V:'+str(dop) + '  E:'+str(el)
                #         cx = x1 - 5
                #         cy = y1 - label_height
                #         cv2.putText(img,third_row,(cx,cy), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale= fontScale,color=(0,255,0),lineType= 1,thickness=thickness)

                # Center Of Mass
                cx = (x2 - x1) // 2 + x1
                cy = (y2 - y1) // 2 + y1

                # Plot arrows at com
                pred_plot = cv2.arrowedLine(pred_plot, (cx, cy), (cx, y1), color=(0, 255, 0), thickness=self.thickness)
                pred_plot = cv2.arrowedLine(pred_plot, (cx, cy), (x2, cy), color=(0, 255, 0), thickness=self.thickness)
                pred_plot = cv2.arrowedLine(pred_plot, (cx, cy), (x1, cy), color=(0, 255, 0), thickness=self.thickness)
                pred_plot = cv2.arrowedLine(pred_plot, (cx, cy), (cx, y2), color=(0, 255, 0), thickness=self.thickness)

            return pred_plot

        else:
            veh_probs = cv2.resize(self.probs[2, :, :], (imToDraw.shape[1], imToDraw.shape[0]))
            pred_plot = self.gen_heatmap(veh_probs, pred_plot, thresh=self.thresh_vehicle, color_map=cv2.COLORMAP_JET, rev=True)

            # pred_plot = cv2.resize(pred_plot, (pred_max.shape[1], pred_max.shape[0]))
            # pred_plot[np.logical_and(np.all(pred_max == (2, 2, 2), axis=-1), probs_max >= self.thresh_vehicle)] = \
            # self.color_palette[1]  # vehicle

        if not state.camera_off:
            pred_plot = cv2.addWeighted(pred_plot, 0.4, imToDraw, 0.6, 0)

        pred_plot = cv2.resize(pred_plot, (1824, 940))
        return pred_plot
