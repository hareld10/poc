from imports import *
import radar_percep_net_v15_mf
from Model import Model
from radarRoadNet import RadarRoadNet


class RadarPedNetMultiFrame(RadarRoadNet):
    def __init__(self, road=False, vehicle=True, weights=None):
        super().__init__()
        self.mode_dict = {"weights": "/weights/radar_percep_rvp_st.v4.0.radar_percep_net_v15_best_model.pth"}

        self.pretrained = False
        self.mf_flag = True
        self.single_frame_mode = False
        self.temporal_window = 3  # [sec] temporal window length
        self.temporal_frames = 8  # number of frames within temporal window
        self.temporal_module_type = 'cnn'  # ['cnn','lstm','transformer']
        self.temporal_filters = 128
        self.temporal_el_upsample = 4
        self.temporal_az_upsample = 2

        self.sant_cut_off_s = 4
        self.sant_cut_off_e = 404

        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]

    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size

        self.model = radar_percep_net_v15_mf.net(output_shape=self.outputShape,
                                                 gpus_list=self.gpus_list,
                                                 batch_size=self.batch_size,
                                                 doppler_ch=48,
                                                 zero_pad=0,
                                                 output_ch=1,
                                                 temporal_frames=self.temporal_frames,
                                                 temporal_module_type=self.temporal_module_type)

        self.model_init()

    def update_state(self, state):
        key = "Radar Ped"
        if key in state.thresh:
            self.thresh = float(state.thresh[key])

    def getRaw(self, S_ANT, x_t, single_frame_mode=False):
        s = self.process_S_ANT(S_ANT)

        with torch.no_grad():
            # modelInput = torch.empty((self.batch_size, *s.shape))
            # modelInput[0] = Variable(s)
            modelInput = torch.unsqueeze(Variable(s).cuda(self.gpus_list[0]), 0)
            if x_t is None:
                single_frame_mode = True
            else:
                x_t = torch.from_numpy(x_t).cuda(self.gpus_list[0])
            pred, new_x_t = self.model(modelInput, x_t, single_frame_mode)

        pred_road = torch.sigmoid(pred[0].squeeze(0)).cpu().numpy()
        return pred_road, new_x_t

    def getIm(self, array, x_t, imToDraw, state, scale):
        self.update_state(state)

        pred_road, new_x_t = self.getRaw(array, x_t)
        scale_x, scale_y = scale
        if self.running_avg_flag:
            pred_road = self.apply_running_avg(pred_road)

        probs_running_avg = pred_road

        # print("Radar: Running Avg Took: {:.4f}".format((time.time() - curStart)))
        # HeatMap
        probsImgRadar = np.stack([probs_running_avg, probs_running_avg, probs_running_avg], axis=2)
        heatMapRadar = np.uint8(exposure.rescale_intensity(np.copy(probs_running_avg), out_range=(255, 0)))
        heatMapRadar = cv2.applyColorMap(heatMapRadar, cv2.COLORMAP_JET)

        # heatMapRadar = cv2.resize(heatMapRadar, None, fx=scale_x, fy=scale_y)
        # probsImgRadar = cv2.resize(probsImgRadar, None, fx=scale_x, fy=scale_y)

        heatMapRadar = cv2.resize(heatMapRadar, (imToDraw.shape[1], imToDraw.shape[0]))
        probsImgRadar = cv2.resize(probsImgRadar, (imToDraw.shape[1], imToDraw.shape[0]))

        if not state.camera_off:
            heatMapRadar = cv2.addWeighted(heatMapRadar, 0.4, imToDraw, 0.6, 0)
            heatMapRadar = np.where(probsImgRadar >= self.thresh, heatMapRadar, imToDraw)
        else:
            heatMapRadar = np.where(probsImgRadar >= self.thresh, heatMapRadar, np.zeros(imToDraw.shape, dtype=np.uint8))
        return heatMapRadar, probsImgRadar, new_x_t.cpu().numpy()
