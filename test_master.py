import time

import pandas as pd
import glob
import cv2
import numpy as np
import torch
print("torch version", torch.__version__)
from AI_Sockets.classes import *
import matplotlib.pyplot as plt
from Manager import Manager
from Master import Master

m = Master()
# m = Manager()
outputShape = (int(940 // 5), int(1824 // 5))
m.init(outputShape)

# base = "/workspace/HDD/200604/200604_drive_1_W36_M8_RF1_ANT3/"
# base = "/mnt/hdd/200624/200624_drive_2_W36_M8_RF1_ANT3/"
base = "/mnt/hdd/200707/ped_near_car_train_W36_M8_RF1/"
df = base + "filtered_data.csv"
df = pd.read_csv(df)
current = df.sample(300).reset_index(drop=True)

# df = df[df['min_score_ped']>0].reset_index(drop=True)
# start = np.random.randint(len(df)-300)
# end = start + 300
# current = df[start:end].reset_index()

for idx in range(len(current)):
    # print(idx)
    rgbImPath = base + "/rgbCamera/rgbCamera_" + str(current.iloc[idx]["timestampCamera"]) + ".png"
    pathRadar = base + "/S_ANT/S_ANT_" + str(current.iloc[idx]["timestampRadar"]) + ".npy"

    rgbImg = cv2.imread(rgbImPath)
    S_ANT = np.load(pathRadar)

    state = AIState()

    # state.screens = ["Radar Road Vehicle", "Camera", "Fusion", "Range Doppler"]
    # state.screens = ["Radar Ped MF", "Range Doppler", None,  None]
    state.screens = ["Combined RVP", "Range Doppler", None]
    # state.screens = ["Radar Ped", "Range Doppler", None, None]
    # state.screens = ["Radar Road Vehicle", "Range Azimuth", None, None]
    # state.screens = ["Radar Perception", "Range Doppler", None, None]

    state.camera_off = False
    state.od_visualization = 0
    state.seg_instance = False
    state.od_flag = True
    state.crop_image = 500

    state.thresh = {}
    state.thresh["Camera Road"] = 0.6
    state.thresh["Radar Road"] = 0.5
    state.thresh["Radar Ped"] = 0.1
    state.thresh["Radar Vehicle"] = 0.6
    state.thresh["Veh Peak"] = 0.6
    state.running_avg = {"Radar": 1, "Camera": 1}

    m.setIm(state=state, ts=time.time(), rd=S_ANT, rgbImg=rgbImg)
    # im = m.getIm(state, S_ANT, rgbImg)

    if idx > 1:
        im = m.getIm()
        if im is not None:
            plt.figure(figsize=(20,10))
            plt.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
            plt.savefig("./images/" + str(idx) +".png")
            plt.close()
        # else:
        #     print("im is None")
    # plt.show()