import sys
import os
import concurrent.futures

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + "/weights/")
sys.path.insert(0, dir_path + "/models/")

from radarRoad import RadarRoad
from cameraRoad import CameraRoad
import numpy as np
import cv2
from skimage import exposure
import time
import pickle
import concurrent.futures
from Manager import Manager
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Process, Queue, Array, Pipe, Value
from multiprocessing import Manager as Manager_MP
from multiprocessing.managers import BaseManager

import psutil
from TemporalModule import TemporalModule


class Master:
    def __init__(self):
        self.q_in = Queue(maxsize=4)
        self.q_out = Queue()
        self.q_order = Queue()

        self.outputShape = None
        self.num_processes = 2
        self.processes = {}
        self.setIm_rate = time.time()
        self.getIm_rate = time.time()
        self.timestamps = Queue()
        self.execuator = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        self.stop_flag = False

        self.connection_map = {}
        self.temporal_module = TemporalModule()
        self.multi_frame = False

    def manager(self, in_queue, out_queue, connection):
        # process = psutil.Process(os.getpid())
        print("Loading process ", os.getpid())
        m = Manager()
        m.init(self.outputShape)

        while True:
            if not in_queue.empty():
                data = in_queue.get(block=True)  # Receive data
                if data is None:
                    print("Got None, breaking ", os.getpid())
                    break
                # print("Master: taking out from in_queue")

                if self.multi_frame:
                    ts = np.array([data[1]])
                    connection.send_bytes(pickle.dumps(np.array(["ts", ts])))
                    time.sleep(0.0005)
                    x_t = None
                    msg = pickle.loads(connection.recv_bytes())
                    if msg[0] == "x_t":
                        x_t = msg[1]
                        if x_t is not None:
                            print("Master: Got GOOD x_t !!", x_t.shape)
                    else:
                        print("Master: Got unknown msg", msg)
                    m.temporal_features_in["Radar Ped MF"] = x_t

                retIm = m.getIm(*data[0])

                if self.multi_frame:
                    if "Radar Ped MF" in m.temporal_features_out:
                        print("Master: Sending New Features to TemporalModule")
                        msg_to_send = np.array(["x_t", m.temporal_features_out["Radar Ped MF"], ts])
                        connection.send_bytes(pickle.dumps(msg_to_send))

                # print("Master: Putting to out_queue after m.getIm")
                out_queue.put((retIm, data[1]), block=False)
                # except Exception as e:
                #     print("Error in manager", e)
                #     exit(1)

            time.sleep(0.01)

    def init(self, outputShape):
        self.outputShape = outputShape
        self.execuator.submit(self.reorder_qout)
        for i in range(self.num_processes):
            parent_conn, child_conn = Pipe()
            self.connection_map[i] = (parent_conn, child_conn)
            self.processes[i] = Process(target=self.manager, args=(self.q_in, self.q_out, self.connection_map[i][1],))
            self.processes[i].start()
        if self.multi_frame:
            self.execuator.submit(self.temporal_module.run, self.connection_map)


    def close(self):
        self.stop_flag = True
        for i in self.processes:
            self.processes[i].terminate()
        self.temporal_module.close()
        return True

    def setIm(self, state, ts, rd, rgbImg):
        # print("Master: Calling setIm FPS: {:.4f}".format(1 / (time.time() - self.setIm_rate)))
        # print("Master: self.q_order ", self.q_order.qsize(), " self.q_in ", self.q_in.qsize(), " self.q_out ", self.q_out.qsize())

        self.setIm_rate = time.time()
        if self.q_in.full():
            # print("Master: q_in Full")
            return

        else:
            self.timestamps.put(ts)
            state.timestamp = ts
            data = ((state, rd, rgbImg), ts)
            self.q_in.put(data, block=False)

    def reorder_qout(self):
        while not self.stop_flag:
            desired_next = self.timestamps.get(block=True)
            while True:
                if not self.q_out.empty():
                    cur_next_data = self.q_out.get(block=False)
                    rgbImg = cur_next_data[0]

                    # font = cv2.FONT_HERSHEY_SIMPLEX
                    # text = str(cur_next_data[1])

                    # # get boundary of this text
                    # textsize = cv2.getTextSize(text, font, 1, 2)[0]

                    # # get coords based on boundary
                    # textX = int((rgbImg.shape[1] - textsize[0]) / 2)
                    # textY = int((textsize[1] + 200))

                    # # add text centered on image
                    # cv2.putText(rgbImg, text, (textX, textY ), font, 1, (255, 255, 255), 2)

                    ts = cur_next_data[1]
                    if desired_next == ts:
                        self.q_order.put(rgbImg)
                        break
                    else:
                        self.q_out.put(cur_next_data)

        return

    def getIm(self):
        # if not self.q_out.empty():
        #     return self.q_out.get(block=False)[0]
        # else:
        #     return None

        if not self.q_order.empty():
            return self.q_order.get(block=False)
        else:
            return None
