from imports import *
import radar_road_net_v2
from Model import Model


class RadarRoadNet(Model):
    def __init__(self, road=False, vehicle=True, weights=None):
        super().__init__()
        self.thresh = 0.6
        self.alpha = 0.4
        self.colorPred = (255, 0, 0)

        # Running avg sequence length
        self.running_avg_flag = False
        self.running_avg_len = 1
        self.running_avg = []
        self.colormap = cv2.COLORMAP_SUMMER

        self.mode_dict = {"weights": "/weights/radar_percep_rvp_st.v3.0.leopard_radar_road_net_v2_best_model.pth"}

        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]

    def model_init(self):
        print("Model: " + str(self.__class__.__name__))
        if self.pretrained:
            print("Loading Model - Weights: ", self.weights)
            self.model.load_state_dict(torch.load(self.weights, map_location=lambda storage, loc: storage))
        self.model = self.model.cuda(self.gpus_list[0])
        self.model.eval()
        return

    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size

        self.model = radar_road_net_v2.net(output_shape=self.outputShape,
                                           gpus_list=self.gpus_list,
                                           batch_size=self.batch_size,
                                           doppler_ch=48,
                                           zero_pad=0,
                                           output_ch=1)

        self.model_init()

    def getRaw(self, S_ANT):
        s = self.process_S_ANT(S_ANT)

        with torch.no_grad():
            # modelInput = torch.empty((self.batch_size, *s.shape))
            # modelInput[0] = Variable(s)
            modelInput = torch.unsqueeze(Variable(s).cuda(self.gpus_list[0]), 0)
            pred = self.model(modelInput)

        pred_road = torch.sigmoid(pred[0].squeeze(0)).cpu().numpy()
        return pred_road

    def update_state(self, state):
        if "Radar Road" in state.thresh:
            self.thresh = float(state.thresh["Radar Road"])

    def getIm(self, array, imToDraw, state, scale):
        self.update_state(state)

        pred_road = self.getRaw(array)
        scale_x, scale_y = scale
        if self.running_avg_flag:
            pred_road = self.apply_running_avg(pred_road)

        probs_running_avg = pred_road

        # print("Radar: Running Avg Took: {:.4f}".format((time.time() - curStart)))
        # HeatMap
        probsImgRadar = np.stack([probs_running_avg, probs_running_avg, probs_running_avg], axis=2)
        heatMapRadar = np.uint8(exposure.rescale_intensity(np.copy(probs_running_avg), out_range=(255, 0)))
        heatMapRadar = cv2.applyColorMap(heatMapRadar, self.colormap)

        # heatMapRadar = cv2.resize(heatMapRadar, None, fx=scale_x, fy=scale_y)
        # probsImgRadar = cv2.resize(probsImgRadar, None, fx=scale_x, fy=scale_y)

        heatMapRadar = cv2.resize(heatMapRadar, (imToDraw.shape[1], imToDraw.shape[0]))
        probsImgRadar = cv2.resize(probsImgRadar, (imToDraw.shape[1], imToDraw.shape[0]))

        if not state.camera_off:
            heatMapRadar = cv2.addWeighted(heatMapRadar, 0.4, imToDraw, 0.6, 0)
            heatMapRadar = np.where(probsImgRadar >= self.thresh, heatMapRadar, imToDraw)
        else:
            heatMapRadar = np.where(probsImgRadar >= self.thresh, heatMapRadar, np.zeros(imToDraw.shape, dtype=np.uint8))
        return heatMapRadar, probsImgRadar
