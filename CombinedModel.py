from imports import *
from Model import Model
import os
from radarMultiSeg import RadarMultiSeg


class CombinedModel(Model):
    def __init__(self, ):
        super().__init__()

        print("Veh Model")
        self.veh_model = RadarMultiSeg(road=True, vehicle=True,
                                       # weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_best_model_200615.pth")
                                        # weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_epoch_4_200615.pth")
                                        weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_200612_epoch_5.pth")
                                        # weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_best_model_200614.pth")

        self.veh_model.init((672, 192))

        print("Road Model")
        self.road_model = RadarMultiSeg(road=True, vehicle=True,
                                        weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_best_model_200615.pth")
                                        # weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_epoch_4_200615.pth")
                                        # weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_200612_epoch_5.pth")
                                        # weights="/weights/radar_road_vehicle_segmentation.v5.0.net_radar_seg_v8_best_model_200614.pth")
        self.road_model.init((672, 192))

    def getIm(self, array, imToDraw, state, seg_instance=False):
        probs, probs_max, pred_max = self.road_model.getRaw(array)

        return self.veh_model.getIm(array, imToDraw.copy(),
                                    state=state,
                                    seg_instance=state.seg_instance,
                                    road_probs=probs[1, :, :])
