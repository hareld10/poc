from AI_Sockets.classes import *
import numpy as np
import base64
from skimage.data import astronaut
import cv2
import time
from concurrent.futures import ThreadPoolExecutor
import zlib
import io
import pickle
import time


class socketServerListener(TCPSocket):
    def __init__(self, host_ip, port, master, parent=None):
        super(socketServerListener, self).__init__(host_ip, port)
        print("Initing Listener")
        self.master = master

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host_ip, self.port))
            s.listen()
            debug = True
            while self.stop_flag == False:
                conn, addr = s.accept()
                with conn:
                    print('Listener: Connected by', addr)
                    while self.stop_flag == False:
                        # print("socketServer: before recv_msg")
                        try:
                            AI_frame = self.recv_msg(conn)
                        except Exception as e:
                            print("Got Exception", e)
                            conn.close()
                            break

                        # print("socketServer: after recv_msg")
                        if not AI_frame:
                            print("socketServer: Got None as AI Frame - Breaking")
                            break

                        rgbImage = AI_frame.image
                        S_ANT = None
                        if (AI_frame.S_ANT is not None):
                            S_ANT = AI_frame.S_ANT

                        # print("Got New ", AI_frame.S_ANT.shape)
                        # self.master.state = AI_frame.state
                        self.master.setIm(AI_frame.state, AI_frame.timestamp, S_ANT, rgbImage)
                        # print("socketServer: self.master.setIm : {:.4f}".format((time.time() - tmp)))
                        time.sleep(0.01)
                    print("socketServer: Exiting in Listener")


class socketServer(TCPSocket):
    def __init__(self, host_ip, port, port_out, master, parent=None):
        print("Initing Sender")
        super(socketServer, self).__init__(host_ip, port_out)
        self.master = master
        self.listener = socketServerListener(host_ip, port, self.master)
        self.executor = ThreadPoolExecutor(max_workers=3)
        self.executor.submit(self.listener.run)

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host_ip, self.port))
            s.listen()
            while (self.stop_flag == False):
                conn, addr = s.accept()
                with conn:
                    print('Sender: Connected by', addr)
                    while (self.stop_flag == False):
                        outputFrame = self.master.getIm()
                        if outputFrame is not None:
                            AI_frame = AIFrame()
                            AI_frame.image = outputFrame
                            AI_frame.input_image_shape = outputFrame.shape
                            # print("socketServer: Sending New Image!")
                            try:
                                self.send_msg(conn, AI_frame)
                            except Exception as e:
                                print("Got Exception", e)
                                conn.close()
                                break

                        # else:
                        #     print("socketServer: Got None from self.master.getIm()")
                        time.sleep(0.01)
        print("Exiting Server")
