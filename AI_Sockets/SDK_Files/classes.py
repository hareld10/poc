import socket
import pickle
import struct

class AIFrame(object):
    image = ""
    S_ANT = None
    input_s_ant_shape = (0,0)
    input_image_shape = (0,0)
    timestamp = 0
    state = None

class AIState(object):
    screens = None
    camera_off = False
    od_visualization = 0
    thresh = None
    running_avg = None
    

class TCPSocket(object):
    def __init__(self, host_ip, port, parent=None):
        self.host_ip = host_ip
        self.port = port
        self.stop_flag = False
        print(self.host_ip)

    def send_msg(self, sock, AI_frame):
        #prefix each message with a 4-byte length (network byte order)
        AISerializedFrame = pickle.dumps(AI_frame, protocol=4)
        #print("len", len(AISerializedFrame))
        msg = struct.pack('>I', len(AISerializedFrame)) + AISerializedFrame
        sock.sendall(msg)

    def recv_msg(self, sock):
        #Read message length and unpack it into an integer
        raw_msglen = self.recvall(sock, 4)
        if not raw_msglen:
            return None
        msglen = struct.unpack('>I', raw_msglen)[0]
        #read the meesage data
        #print("length is: ", msglen)
        data = self.recvall(sock, msglen)
        AI_frame = pickle.loads(data)
        return AI_frame

    def recvall(self, sock, n):
        #Helper function to recv n bytes or return None if EOF is hit
        data = b''
        while(len(data) < n):
            packet = sock.recv(n - len(data))
            if not packet:
                return None
            data += packet
        return data

    def stop(self):
        tmp = not self.stop_flag
        self.stop_flag = True
        return tmp
