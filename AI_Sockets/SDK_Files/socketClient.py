#!/usr/bin/env python3

from AI_Sockets.classes import *
import queue
from contextlib import closing
from concurrent.futures import ThreadPoolExecutor
import time

class socketClient(TCPSocket):
    def __init__(self, host_ip, port, sender=None, parent=None):
        super(socketClient, self).__init__(host_ip, port)
        self.q_in = queue.Queue(maxsize=4)
        self.q_out = queue.Queue(maxsize=15)
        self.executor = ThreadPoolExecutor(max_workers=3)
        self.sender = sender
        
    def outQ(self):
        # print("socketClient: initing outQ")
        #self.q_out.clear()
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
            s.connect((self.host_ip, self.port))
            while self.stop_flag == False:
                AI_frame = self.recv_msg(s)
                if AI_frame is not None:
                    if self.q_out.full():
                        print("socketClient: Warning, q_out is full")
                    else:
                        self.q_out.put(AI_frame, block=False)
                time.sleep(0.01)         
            s.close()
        # print("socketClient: Leaving outQ")
    
    def inQ(self):
        # print("socketClient: initing inQ")
        #self.q_in.clear()
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
            s.connect((self.host_ip, self.port))
            while self.stop_flag == False:
                #print("socketClient: In inQ")
                try:
                    if not self.q_in.empty():
                        AI_frame = self.q_in.get(block=False)
                        AI_frame.timestamp = str(time.time())
                        print("socketClient: Sending new AI_Frame ", AI_frame.S_ANT.shape)
                        self.send_msg(s, AI_frame)
                    # else:
                    #     print("socketClient: q_in empty")
                        #print("socketClient: self.send_msg(s, AI_frame) : {:.4f}".format((time.time() - AI_frame.timestamp )))
                except Exception as e:
                        print("socketClient: Got Exception", e)
                time.sleep(0.01)  

            # print("socketClient: inQ: Connection closed")
            s.close()
        # print("socketClient: Leaving inQ")
        
    def run(self):
        self.stop_flag = False
        if self.sender:
            self.executor.submit(self.inQ)
        else:
            self.executor.submit(self.outQ)
        
