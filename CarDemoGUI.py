from BaseGui import *
from AI_Sockets.classes import AIState
from PyQt4.QtGui import QComboBox, QWidget, QVBoxLayout, QDoubleSpinBox, QSpinBox


class Ui_DemoWindow(Ui_MainWindow):
    def setupUi(self, MainWindow):
        # Apply Multi Label
        super(Ui_DemoWindow, self).setupUi(MainWindow)
        app.aboutToQuit.connect(self.closeEvent)

        self.ai_state = AIState()
        # Add Thresholds
        self.ai_state.thresh = {}
        self.ai_state.screens = [None]*4
        self.ai_state.running_avg = {}

        # Set Range Doppler 3D
        self.radar_view_combobox.setCurrentIndex(4)

        # set ai_flag True
        self.camera_ai_checkbox.setChecked(True)
        self.cameraAIFlagCheckbox_changed(self.camera_ai_checkbox)

        # Remove
        self.camera_parameters_layout.removeWidget(self.camera_apply_button)
        self.camera_parameters_layout.removeWidget(self.camera_underparameters_label)

        # Camera BackGround
        self.camera_bg_label = QtGui.QLabel("Camera Off:")
        self.camera_bg_checkbox = QtGui.QCheckBox(self.camera_parameters_widget)
        self.camera_bg_checkbox.clicked.connect(partial(self.camera_bg_changed, self.camera_bg_checkbox))
        self.camera_parameters_layout.addWidget(self.camera_bg_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.camera_bg_checkbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)

        self.radar_scenery_label = QtGui.QLabel("Radar Scenery:")
        self.radar_scenery_checkbox = QtGui.QCheckBox(self.camera_parameters_widget)
        self.radar_scenery_checkbox.clicked.connect(partial(self.radar_scenery_changed, self.radar_scenery_checkbox))
        self.camera_parameters_layout.addWidget(self.radar_scenery_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.radar_scenery_checkbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)

        # Screens //todo Add Default View
        self.screen0_label = QtGui.QLabel("Screen0:")
        self.screen0_combo = ComboInputs(index=0, top=self, default="Radar Road Vehicle")
        self.camera_parameters_layout.addWidget(self.screen0_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.screen0_combo, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)

        self.screen1_label = QtGui.QLabel("Screen1:")
        self.screen1_combo = ComboInputs(index=1, top=self, default="Camera")
        self.camera_parameters_layout.addWidget(self.screen1_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.screen1_combo, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)

        self.screen2_label = QtGui.QLabel("Screen2:")
        self.screen2_combo = ComboInputs(index=2, top=self, default="Fusion")
        self.camera_parameters_layout.addWidget(self.screen2_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.screen2_combo, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)

        self.screen3_label = QtGui.QLabel("Screen3:")
        self.screen3_combo = ComboInputs(index=3, top=self, default="Range Doppler")
        self.camera_parameters_layout.addWidget(self.screen3_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.screen3_combo, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)

        #### Camera ####
        self.radar_spinbox_label = QtGui.QLabel("Th radar road")
        self.radar_spinbox = SpinThresh("Radar Road", self)
        self.camera_parameters_layout.addWidget(self.radar_spinbox_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.radar_spinbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)
        self.ai_state.thresh["Radar Road"] = 0.65 # Set Deafault Value

        self.radar_vehicle_spinbox_label = QtGui.QLabel("Th radar vehicle")
        self.radar_vehicle_spinbox = SpinThresh("Radar Vehicle", self)
        self.camera_parameters_layout.addWidget(self.radar_vehicle_spinbox_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.radar_vehicle_spinbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)
        self.ai_state.thresh["Radar Vehicle"] = 0.65 # Set Deafault Value

        self.radar_ra_spinbox_label = QtGui.QLabel("Radar running-avg")
        self.radar_ra_spinbox = SpinThreshInt("Radar", self)
        self.camera_parameters_layout.addWidget(self.radar_ra_spinbox_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.radar_ra_spinbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)
        self.ai_state.running_avg["Radar"] = 1 # Set Deafault Value


        #### Camera ####
        self.camera_spinbox_label = QtGui.QLabel("Th camera")
        self.camera_spinbox = SpinThresh("Camera", self)
        self.camera_parameters_layout.addWidget(self.camera_spinbox_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.camera_spinbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)
        self.ai_state.thresh["Camera"] = 0.65

        self.camera_ra_spinbox_label = QtGui.QLabel("Camera running-avg")
        self.camera_ra_spinbox = SpinThreshInt("Camera", self)
        self.camera_parameters_layout.addWidget(self.camera_ra_spinbox_label, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.camera_ra_spinbox, self.camera_parameters_layout.rowCount()-1, 1, 1, 2)
        self.ai_state.running_avg["Camera"] = 1 # Set Deafault Value

        # Add button Back
        self.camera_parameters_layout.addWidget(self.camera_apply_button, self.camera_parameters_layout.rowCount(), 0, 1, 1)
        self.camera_parameters_layout.addWidget(self.camera_underparameters_label, self.camera_parameters_layout.rowCount(), 0, 1, 3)

        self.camera_ai_checkbox.clicked.connect(partial(self.cameraAIFlagCheckbox_changed_ai, self.camera_ai_checkbox))
        self.update_state()

    def stream(self, type, source, action, host_id = WiseSDK.SERVER_ID_E.SERVER_ID_REMOTEHOST):
        ret = super().stream(type, source, action, host_id)
        self.radar_min_range_LineEdit.setText("0")
        self.radar_max_range_LineEdit.setText("150")
        self.radar_apply_button_clicked()
        return ret

    def update_state(self):
        self.camera_mayavi_widget.ai_state = self.ai_state
        
    def radar_scenery_changed(self, obj):
        self.ai_state.radar_scenery = obj.isChecked()
        self.update_state()

    def camera_bg_changed(self, obj):
        self.ai_state.camera_off = obj.isChecked()
        self.update_state()

    def cameraAIFlagCheckbox_changed_ai(self, obj):
        self.camera_mayavi_widget.setAIFlag(obj.isChecked())
        self.camera_bg_label.setVisible(obj.isChecked())
        self.camera_bg_checkbox.setVisible(obj.isChecked())


    def retranslateUi(self, MainWindow):
        super(Ui_DemoWindow, self).retranslateUi(MainWindow)

class ComboInputs(QWidget):
   def __init__(self, index, top, default, parent = None):
      super(ComboInputs, self).__init__(parent)
      layout = QVBoxLayout()
      self.index = index
      self.top = top

      self.cb = QComboBox()
      self.screens_options = ["None", "Radar Road Vehicle", "Camera", "Fusion", "Range Doppler", "Radar Combine Heatmap"]
      self.cb.addItems(self.screens_options)
      self.cb.currentIndexChanged.connect(self.selectionchange)
      self.cb.setCurrentIndex(self.screens_options.index(default))
      layout.addWidget(self.cb)
      self.setLayout(layout)

   def selectionchange(self,i):
      if i == 0:
        self.top.ai_state.screens[self.index] = None
      else:
        self.top.ai_state.screens[self.index] = self.cb.currentText()
      self.top.update_state()


class SpinThresh(QWidget):
   def __init__(self, index, top, parent = None):
        super(SpinThresh, self).__init__(parent)
        self.index = index
        self.top = top
        layout = QVBoxLayout()
        self.sp = QDoubleSpinBox()
        self.sp.setRange(0, 1)
        self.sp.setSingleStep(0.02)
        self.sp.setValue(0.55)
        layout.addWidget(self.sp)
        self.sp.valueChanged.connect(self.valuechange)
        self.setLayout(layout)

   def valuechange(self):
        self.top.ai_state.thresh[self.index] = self.sp.value()
        self.top.update_state()


class SpinThreshInt(QWidget):
   def __init__(self, input_name, top, parent = None):
        super(SpinThreshInt, self).__init__(parent)
        self.input_name = input_name
        self.top = top
        layout = QVBoxLayout()
        self.sp = QSpinBox()
        self.sp.setRange(0, 5)
        self.sp.setValue(1)
        layout.addWidget(self.sp)
        self.sp.valueChanged.connect(self.valuechange)
        self.setLayout(layout)

   def valuechange(self):
        self.top.ai_state.running_avg[self.input_name] = self.sp.value()
        self.top.update_state()

if __name__ == "__main__":
    import sys
    #app = QtGui.QApplication(sys.argv)
    app = QtGui.QApplication.instance()
    MainWindow = QtGui.QMainWindow()
    ui = Ui_DemoWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

