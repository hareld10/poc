from Master import Master
from AI_Sockets.classes import *
from AI_Sockets.socketServer import socketServer


if __name__ ==  "__main__":
	master = Master()
	outputShape = (int(940//5),int(1824//5))
	master.init(outputShape)
	socket_server = socketServer("172.17.0.3", 54010, 54011, master)
	socket_server.run()
