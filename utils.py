from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
from scipy import signal
import numpy as np
import torch
import pandas as pd
import os
from tqdm import tqdm_notebook as tqdm


def detect_peaks(image, peak_threshold):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """

    # Filter pixels below threshold
    image[image < peak_threshold] = 0

    # define an 8-connected neighborhood
    neighborhood = generate_binary_structure(3, 3)

    # apply the local maximum filter; all pixel of maximal value
    # in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood) == image
    # local_max is a mask that contains the peaks we are
    # looking for, but also the background.
    # In order to isolate the peaks we must remove the background from the mask.

    # we create the mask of the background
    background = (image == 0)

    # a little technicality: we must erode the background in order to
    # successfully subtract it form local_max, otherwise a line will
    # appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    # we obtain the final mask, containing only peaks,
    # by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    return detected_peaks


def box_nms(row, col, pred, nms_thresh, bb_thresh, bb_type):
    scores = np.zeros(len(row))
    bb = np.zeros([len(row), 4])

    for i in range(bb.shape[0]):

        if bb_type == 'ped':

            if row[i] == 0 or col[i] == 0 or row[i] == pred.shape[0] or col[i] == pred.shape[1]:
                continue

            # Create bb
            bb[i, 0] = col[i] - np.argmax(np.flip(pred[row[i], :col[i]], axis=0) < bb_thresh)  # x1 (left)
            bb[i, 1] = row[i] - np.argmax(np.flip(pred[:row[i], col[i]], axis=0) < bb_thresh)  # y1 (top)
            bb[i, 2] = col[i] + np.argmax(pred[row[i], col[i]:] < bb_thresh)  # x2 (right)
            bb[i, 3] = row[i] + np.argmax(pred[row[i]:, col[i]] < bb_thresh)  # y2 (bottom)
            scores[i] = pred[row[i], col[i]]

        elif bb_type == 'veh':
            bb[i, 0] = int(col[i] - pred[1, row[i], col[i]] - pred[3, row[i], col[i]] // 2)
            bb[i, 1] = int(row[i] - pred[2, row[i], col[i]] - pred[4, row[i], col[i]] // 2)
            bb[i, 2] = int(col[i] - pred[1, row[i], col[i]] + pred[3, row[i], col[i]] // 2)
            bb[i, 3] = int(row[i] - pred[2, row[i], col[i]] + pred[4, row[i], col[i]] // 2)
            scores[i] = pred[0, row[i], col[i]]

    x1 = bb[:, 0]
    y1 = bb[:, 1]
    x2 = bb[:, 2]
    y2 = bb[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]  # get boxes with more ious first

    keep = []
    while order.size > 0:
        i = order[0]  # pick maximum iou box
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)  # maximum width
        h = np.maximum(0.0, yy2 - yy1 + 1)  # maximum height
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= nms_thresh)[0]
        order = order[inds + 1]

    return bb[keep, :]



def non_max_suppression_fast(boxes, overlapThresh=0.3):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []
    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")
    # initialize the list of picked indexes
    pick = []
    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))
    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick].astype("int")


def sort_bb(bb):
    x1_vec = bb[:, 0].argsort()
    sorted_bb = np.zeros([bb.shape[0], 4])
    for i in range(bb.shape[0]):
        sorted_bb[i, :] = bb[x1_vec[i], :]

    return sorted_bb


""


def box_nms_heatmap(row, col, pred, nms_thresh, bb_thresh):
    scores = np.zeros(len(row))
    bb = np.zeros([len(row), 5])

    for i in range(bb.shape[0]):

        if row[i] == 0 or col[i] == 0 or row[i] == pred.shape[0] or col[i] == pred.shape[1]:
            continue

        # Create bb
        bb[i, 0] = col[i] - np.argmax(np.flip(pred[row[i], :col[i]], axis=0) < bb_thresh)  # x1 (left)
        bb[i, 1] = row[i] - np.argmax(np.flip(pred[:row[i], col[i]], axis=0) < bb_thresh)  # y1 (top)
        bb[i, 2] = col[i] + np.argmax(pred[row[i], col[i]:] < bb_thresh)  # x2 (right)
        bb[i, 3] = row[i] + np.argmax(pred[row[i]:, col[i]] < bb_thresh)  # y2 (bottom)

        scores[i] = pred[row[i], col[i]]

        w = np.abs(bb[i, 2] - bb[i, 0])
        h = np.abs(bb[i, 3] - bb[i, 1])

        if h > w:
            bb[i, 0] = 0
            bb[i, 1] = 0
            bb[i, 2] = 0
            bb[i, 3] = 0
            scores[i] = 0
        #             bb[i,1] = row[i] - w//2
        #             bb[i,3] = row[i] + w//2
        #             scores[i] = np.min(scores[scores>0])
        if w / h > 3:
            bb[i, 0] = col[i] - h * 3 // 2
            bb[i, 2] = col[i] + h * 3 // 2
            scores[i] = np.min(scores[scores > 0])
        bb[i, 4] = scores[i]

    x1 = bb[:, 0]
    y1 = bb[:, 1]
    x2 = bb[:, 2]
    y2 = bb[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]  # get boxes with more ious first

    keep = []
    while order.size > 0:
        i = order[0]  # pick maximum iou box
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)  # maximum width
        h = np.maximum(0.0, yy2 - yy1 + 1)  # maximum height
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= nms_thresh)[0]
        order = order[inds + 1]

    return bb[keep, :]