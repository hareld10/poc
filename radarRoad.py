from imports import *
import netRRD_v2
from Model import Model


class RadarRoad(Model):
    def __init__(self, road=True, vehicle=False, weights=None):
        super().__init__()
        self.thresh = 0.6
        self.alpha = 0.4
        self.colorPred = (255, 0, 0)
        self.curMask = np.ascontiguousarray(np.zeros((600, 48 * 2, 48), dtype="float32"))

        # Running avg sequence length
        self.running_avg_len = 1
        self.running_avg = []

        self.road_dict = {"weights": "/weights/RRoadSeg.v2.1.netRRD_2_best_model_061219.pth"}
        self.vehicle_dict = {"weights": "/weights/radar_vehicle_segmentation.v1.3.2.net_radar_seg_v4_best_model.pth"}

        self.mode = "road"
        if road:
            self.mode = "road"
            self.mode_dict = self.road_dict
        elif vehicle:
            self.mode = "vehicle"
            self.mode_dict = self.vehicle_dict

        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]

    def model_init(self):
        self.model.load_state_dict(torch.load(self.weights, map_location=lambda storage, loc: storage))
        self.model = self.model.cuda(self.gpus_list[0])
        self.model.eval()
        print("Model: ", self.mode, " - Weights: ", self.weights)
        return

    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size

        if self.mode == "road":
            self.model = netRRD_v2.net()
        elif self.mode == "vehicle":
            # update to vehicle net
            self.model = netRRD_v2.net()

        self.model_init()

    def process_S_ANT(self, S_ANT):
        S_ANT = S_ANT[:600, :, :]
        # print("S_ANT shape ", S_ANT.shape)
        self.curMask[:, ::2, :] = np.array(S_ANT.real)
        self.curMask[:, 1::2, :] = np.array(S_ANT.imag)
        s = np.transpose(self.curMask, (2, 0, 1))
        s = cv2.resize(s, self.outputShape)
        return np.transpose(s, (2, 0, 1))

    def getRaw(self, S_ANT):
        s = self.process_S_ANT(S_ANT)

        with torch.no_grad():
            modelInput = torch.unsqueeze(Variable(torch.from_numpy(s)).cuda(self.gpus_list[0]), 0)
            pred = torch.squeeze(self.model(modelInput))
            probs = torch.sigmoid(pred)

            # 0/1 Visualiztion
            #             probs[probs < self.thresh] = 0
            #             probs[probs >= self.thresh] = 1
            # probs = probs.cpu().numpy().astype(np.uint8)
            probs = probs.cpu().numpy()
            return probs

    def getIm(self, array, imToDraw):
        probs = self.getRaw(array)
        # Running avg

        self.running_avg.append(probs)
        if len(self.running_avg) > self.running_avg_len:
            self.running_avg.pop(0)

        probs_running_avg = np.zeros(probs.shape)
        for i in range(len(self.running_avg)):
            probs_running_avg += self.running_avg[i]

        probs_running_avg /= len(self.running_avg)

        # print("Radar: Running Avg Took: {:.4f}".format((time.time() - curStart)))
        # HeatMap
        probsImgRadar = np.stack([probs_running_avg, probs_running_avg, probs_running_avg], axis=2)
        heatMapRadar = np.uint8(exposure.rescale_intensity(np.copy(probs_running_avg), out_range=(255, 0)))
        heatMapRadar = cv2.applyColorMap(heatMapRadar, cv2.COLORMAP_JET)
        if self.camera_bg:
            heatMapRadar = cv2.addWeighted(heatMapRadar, 0.4, imToDraw, 0.6, 0)
            heatMapRadar = np.where(probsImgRadar > self.thresh, heatMapRadar, imToDraw)
        else:
            heatMapRadar = np.where(probsImgRadar > self.thresh, heatMapRadar, np.zeros(imToDraw.shape, dtype=np.uint8))
        return heatMapRadar, probs_running_avg
