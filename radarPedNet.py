from imports import *
import radar_percep_net_v15
from Model import Model
from radarRoadNet import RadarRoadNet


class RadarPedNet(RadarRoadNet):
    def __init__(self, road=False, vehicle=True, weights=None):
        super().__init__()
        self.mode_dict = {"weights":
                              "/weights/radar_percep_rvp_st.v4.0.radar_percep_net_v15_200713_epoch_6.pth"
                            # "/weights/radar_percep_rvp_st.v4.0.radar_percep_net_v15_scenearios_epoch_5.pth"
                            # "/weights/radar_percep_rvp_st.v4.0.radar_percep_net_v15_scenearios_epoch_4.pth"
                          }

        self.mf_flag = False
        self.temporal_window = 3  # [sec] temporal window length
        self.temporal_frames = 8  # number of frames within temporal window
        self.temporal_module_type = 'cnn'  # ['cnn','lstm','transformer']
        self.temporal_filters = 128
        self.temporal_el_upsample = 4
        self.temporal_az_upsample = 2

        self.sant_cut_off_s = 4
        self.sant_cut_off_e = 404

        self.colormap = cv2.COLORMAP_JET
        if weights:
            self.weights = weights
        else:
            # Define Defaults weights
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]

    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size

        self.model = radar_percep_net_v15.net(output_shape=self.outputShape,
                                              gpus_list=self.gpus_list,
                                              batch_size=self.batch_size,
                                              doppler_ch=48,
                                              zero_pad=0,
                                              output_ch=1,
                                              temporal_frames=self.temporal_frames,
                                              temporal_module_type=self.temporal_module_type)

        self.model_init()

    def update_state(self, state):
        key = "Radar Ped"
        if key in state.thresh:
            self.thresh = float(state.thresh[key])
