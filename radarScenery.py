from imports import *
import net_radar_seg_v8_2
from Model import Model
from radarMultiSeg import RadarMultiSeg
import time
import gc


class RadarScenery(RadarMultiSeg):
    def __init__(self, classes=[0, 1, 2], weights=None):
        super().__init__()
        
        self.color_palette = [(107,142, 35),   # vegetation
                             (70,130,180),    # sky
                             (241, 230, 255)] # structure

        self.road_vehicle_dict = {"weights": "/weights/radar_scenary_seg.v1.1.net_radar_seg_v8_2_epoch_1_200112.pth"}

        # Default Thresholds
        self.threshes = [0.55]*6
        self.classes = classes

        self.mode = "radar_scenery"
        self.mode_dict = self.road_vehicle_dict

        if weights:
            self.weights = weights
        else:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            self.weights = dir_path + "/" + self.mode_dict["weights"]


    def init(self, outputShape, batch_size=1):
        self.outputShape = outputShape
        self.batch_size = batch_size
        self.model = net_radar_seg_v8_2.net(self.outputShape, self.gpus_list, 1)  

        self.model_init()


    def getIm(self, array, imToDraw, mask=None):
        self.probs, probs_max, pred_max = self.getRaw(array)
        
        pred_max = np.stack((pred_max,pred_max,pred_max),axis=2)
        
        pred_plot = np.zeros(imToDraw.shape, dtype=imToDraw.dtype)

        for i in self.classes:
            pred_plot[np.logical_and(np.all(pred_max==(i+1,i+1,i+1),axis=-1), probs_max>=self.thresh_road)] = self.color_palette[i] # road
        
        return pred_plot
        
