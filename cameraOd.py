from imports import *
from Model import Model
import os
import time

import torch
from torch.backends import cudnn
import json
import sys

import pathlib

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)

from models.backbone import EfficientDetBackbone

import cv2
import numpy as np
import os
from tqdm import tqdm
import glob

from models.efficientdet.utils import BBoxTransform, ClipBoxes
from models.utils.utils import preprocess_original, invert_affine, postprocess, resize

cudnn.fastest = True
cudnn.benchmark = True


class CameraObjectDetection(Model):
    def __init__(self, weights=None):
        super().__init__()
        self.compound_coef = 4
        self.threshold = 0.2
        self.iou_threshold = 0.2
        self.input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536]
        self.obj_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat',
                         'traffic light',
                         'fire hydrant', '', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse',
                         'sheep',
                         'cow', 'elephant', 'bear', 'zebra', 'giraffe', '', 'backpack', 'umbrella', '', '', 'handbag',
                         'tie',
                         'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat',
                         'baseball glove',
                         'skateboard', 'surfboard', 'tennis racket', 'bottle', '', 'wine glass', 'cup', 'fork', 'knife',
                         'spoon',
                         'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
                         'donut',
                         'cake', 'chair', 'couch', 'potted plant', 'bed', '', 'dining table', '', '', 'toilet', '',
                         'tv',
                         'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink',
                         'refrigerator', '', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
                         'toothbrush']

        self.model = EfficientDetBackbone(compound_coef=self.compound_coef, num_classes=len(self.obj_list))
        self.model.load_state_dict(torch.load(f'./weights/efficientdet-d{self.compound_coef}.pth'))
        self.model.requires_grad_(False)
        self.model.eval()
        self.model = self.model.cuda(self.gpus_list[0])
        print("Model: EfficientDet D4 has been loaded")
        self.classes_plt = {"car": (255, 0, 0), "truck": (255, 191, 0), "bus": (250, 206, 135),
                            "motorcycle": (238, 104, 123),
                            "pedestrian": (0, 165, 255), "person": (0, 165, 255),
                            "traffic light": (51, 0, 204)}


    def display(self, preds, img):
        for j in range(len(preds['rois'])):
            obj = self.obj_list[preds['class_ids'][j]]
            if obj == "person":
                obj = 'pedestrian'
            if obj in self.classes_plt:
                (x1, y1, x2, y2) = preds['rois'][j].astype(np.int)

                score = float(preds['scores'][j])
                color = self.classes_plt[obj]
                cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)
                cv2.putText(img, '{}, {:.3f}'.format(obj, score),
                            (x1, y1 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            (255, 255, 0), 1)
        return img

    def getIm(self, im, im_to_draw, state):
        mean = (0.0, 0.0, 0.0)
        std = (1.0, 1.0, 1.0)

        ori_imgs = [im]
        normalized_imgs = [(img / 255 - mean) / std for img in ori_imgs]
        imgs_meta = [resize(img[..., ::-1]) for img in normalized_imgs]
        framed_imgs = [img_meta for img_meta in imgs_meta]

        x = torch.stack([torch.from_numpy(fi).cuda(self.gpus_list[0]) for fi in framed_imgs], 0)
        x = x.to(torch.float32).permute(0, 3, 1, 2)

        with torch.no_grad():
            features, regression, classification, anchors = self.model(x)

            regressBoxes = BBoxTransform()
            clipBoxes = ClipBoxes()

            out = postprocess(x, anchors, regression, classification,
                              regressBoxes, clipBoxes,
                              self.threshold, self.iou_threshold)

        return self.display(out[0], im_to_draw)