import sys
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + "/weights/")
sys.path.insert(0, dir_path + "/models/")

from radarMultiSeg import RadarMultiSeg
from radarVehNet import RadarVehNet
from radarRoadNet import RadarRoadNet
from cameraMultiSeg import CameraMultiSeg
from cameraOd import CameraObjectDetection
from CombinedModel import CombinedModel
from radarPedNet import RadarPedNet
from radarPedNet_mf import RadarPedNetMultiFrame

import numpy as np
import cv2
from skimage import exposure
import time
import concurrent.futures
import torch
from PIL import Image
from PIL import ImageFont, ImageDraw, ImageOps


class Manager:
    def __init__(self):
        self.radar_road = None
        self.camera_multi_seg = None
        self.radar_road_vehicle, self.radar_veh_net, self.radar_road_net = None, None, None
        self.camera_od, self.combined_model, self.radar_ped_net, self.radar_ped_net_mf = None, None, None, None
        self.outputShape = None
        self.state = None
        self.full_res_rgb = None
        self.imph, self.impw, self.c = 0, 0, 3
        self.imh, self.imw = 0, 0
        self.scale_x, self.scale_y = 1, 1

        self.fix_w, self.fix_h = 910, 470

        self.temporal_features_out = {}
        self.temporal_features_in = {}

        self.downPass = 35
        self.padFromAbove = 80
        self.padBetween = 100
        self.padFromTheSide = 0

        self.cur_dtype = np.uint8

        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.color = (243, 150, 33)

        self.color_palette = [(255, 0, 0),  # road
                              (0, 0, 255),  # veh
                              (0, 255, 0),  # ped
                              (107, 142, 35),  # vegetation
                              (70, 130, 180),  # sky
                              (241, 230, 255)]  # structure
        self.screens_output = {}

        # Fusion
        self.p_r, self.p_c, self.rd, self.finalIm, self.rgbImg, self.cur_bb = None, None, None, None, None, None
        self.titles = {"Radar Road Vehicle": "Radar Perception",
                       "Camera": "Camera Perception", "Fusion": "Sensor Fusion",
                       "Range Doppler": "Radar Data",
                       "Radar Road Heatmap": "Radar Road Heatmap",
                       "Radar Vehicle Heatmap": "Radar Vehicle Heatmap",
                       "Radar Combine Heatmap": "Radar Heatmap",
                       "Radar Vehicle": "Radar Vehicle",
                       "Radar Road": "Radar Road",
                       "Radar Ped": "Radar Ped",
                       "Radar Ped MF": "Radar Ped MF",
                       "Radar Perception": "Radar Perception",
                       "Range Azimuth": "Radar Data",
                       "Combined RVP": "Radar Perception"}

        self.recording_path = "/media/nvidia/wiseData/video/"
        self.fusion_frame = None
        self.frame_data = None
        self.classes_plt = {"car": (255, 0, 0), "truck": (255, 191, 0), "bus": (250, 206, 135),
                            "motorcycle": (238, 104, 123),
                            "pedestrian": (0, 165, 255), "person": (0, 165, 255),
                            "traffic light": (51, 0, 204)}
        return

    def init(self, outputShape):
        self.outputShape = outputShape

        # Camera Models
        # self.camera_multi_seg = CameraMultiSeg()
        # self.camera_multi_seg.init()
        # self.camera_od = CameraObjectDetection()

        # self.radar_scenery = RadarScenery()

        # self.radar_veh_net = RadarVehNet()
        # self.radar_road_net = RadarRoadNet()
        # self.radar_veh_net.init(self.outputShape)
        # self.radar_road_net.init(self.outputShape)

        # Radar Multi Seg
        # self.radar_road_vehicle = RadarMultiSeg(road=True, vehicle=True)
        # self.radar_road_vehicle.init((672, 192))

        self.combined_model = CombinedModel()

        self.radar_ped_net = RadarPedNet()
        self.radar_ped_net.init((96, 224))

        # Multi Frame
        # self.radar_ped_net_mf = RadarPedNetMultiFrame()
        # self.radar_ped_net_mf.init((96, 224))

        self.imh, self.imw = self.outputShape
        return True

    def getFusionMulti(self, p1, p2, imToDraw):
        if p1 is None or p2 is None:
            print("Manager-getFusionMulti: Some is None")
            return

        fusion_ratio, fusion_conf = 0.6, 0.6

        if p1.shape != p2.shape:
            p1 = np.transpose(p1, (1, 2, 0))
            p1 = cv2.resize(p1, (p2.shape[2], p2.shape[1]))
            p1 = np.transpose(p1, (2, 0, 1))
        probs_fusion = p1 * fusion_ratio + p2 * (1 - fusion_ratio)

        # probs_max,pred_max = torch.max(torch.from_numpy(probs_fusion),axis=0)

        # probs_max = probs_max.cpu().numpy()
        # pred_max = pred_max.cpu().numpy()

        probs_max = np.max(probs_fusion, axis=0)
        pred_max = np.argmax(probs_fusion, axis=0)

        pred_thresh = np.copy(pred_max)
        # pred_thresh[probs_max<0.6] = 0

        pred_thresh = np.stack((pred_thresh, pred_thresh, pred_thresh), axis=2)
        pred_plot = np.copy(imToDraw)

        for j in range(len(self.color_palette)):
            pred_plot[np.all(pred_thresh == (j + 1, j + 1, j + 1), axis=-1)] = self.color_palette[j]

        pred_plot = cv2.addWeighted(pred_plot, 0.4, imToDraw, 0.6, 0)
        return pred_plot

    @staticmethod
    def getFusionSingle(probs_running_avg, c_probs_running_avg, rgbImg):
        if probs_running_avg is None or c_probs_running_avg is None:
            return
        fusion_ratio, fusion_conf = 0.6, 0.6
        probs_fusion = probs_running_avg * fusion_ratio + c_probs_running_avg * (1 - fusion_ratio)
        probsImg_fusion = np.stack([probs_fusion, probs_fusion, probs_fusion], axis=2)
        heatMap_fusion = np.uint8(exposure.rescale_intensity(np.copy(probs_fusion), out_range=(255, 0)))
        heatMap_fusion = cv2.applyColorMap(heatMap_fusion, cv2.COLORMAP_JET)
        heatMap_fusion = cv2.addWeighted(heatMap_fusion, 0.4, rgbImg, 0.6, 0)
        heatMap_fusion = np.where(probsImg_fusion > fusion_conf, heatMap_fusion, rgbImg)
        return heatMap_fusion

    @staticmethod
    def scale(x, out_range=(0, 255), axis=None):
        domain = np.min(x, axis), np.max(x, axis)
        y = (x - (domain[1] + domain[0]) / 2) / (domain[1] - domain[0])
        return y * (out_range[1] - out_range[0]) + (out_range[1] + out_range[0]) / 2

    def getRD(self, rd):
        z = np.sum(np.abs(rd), axis=1)
        z = np.fft.fftshift(z, 1).T
        rdz = 20 * np.log10(z)
        rdz = np.uint8(exposure.rescale_intensity(np.copy(rdz), out_range=(0, 255)))
        rdz = cv2.applyColorMap(rdz, cv2.COLORMAP_JET)
        rdz = cv2.resize(rdz, (self.imw, self.imh))
        # rdz = self.add_text(rdz, x="Range", y="Dop")
        return rdz

    def getRA(self, rdae):
        # rdae (96, 600, 6, 21)
        ffta = np.zeros([rdae.shape[0] // 2, rdae.shape[1], rdae.shape[2], rdae.shape[3]])

        for i in range(rdae.shape[0] // 2):
            ffta[i, :, :, :] = np.sqrt(rdae[2 * i, :, :, :] ** 2 + rdae[2 * i + 1, :, :, :] ** 2)

        rng_az = np.sum(ffta, axis=(0, 2)).T
        # rng_az = cv2.resize(rng_az, (300, 800))
        # rng_az (21, 600)
        rng_az = rng_az[:, 4:]

        rng_az = 20 * np.log10(rng_az)
        rng_az = np.uint8(exposure.rescale_intensity(np.copy(rng_az), out_range=(0, 255)))
        rng_az = cv2.applyColorMap(rng_az, cv2.COLORMAP_JET)
        return rng_az

    def add_text(self, im, x, y):
        template = np.zeros((im.shape[0] + 40, im.shape[1] + 40, 3), dtype=im.dtype)

        pad_text_side = 20
        pad_text_down = 20

        template[:im.shape[0], :im.shape[1]] = im
        thickness = 2
        txt_size = 1

        # X axis
        textsize = cv2.getTextSize(x, self.font, txt_size, thickness)[0]
        textY = int((template.shape[0] + textsize[1]) / 2)

        template = cv2.putText(template, x, (10, textY), self.font, txt_size, self.color, thickness, self.font)

        # Y axis
        textsize = cv2.getTextSize(y, self.font, txt_size, thickness)[0]
        textX = int((template.shape[1] - textsize[0]) / 2)

        template = cv2.putText(template, x, (textX, -10), self.font, txt_size, self.color, thickness, self.font)
        return template

    def set_screen_1(self, padded, im, text):
        im = cv2.resize(im, (910, 470))
        imh, imw, c = im.shape
        padded[-imh - self.downPass:-self.downPass, :imw, :] = im

        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((imw - textsize[0]) / 2)
        textY = int(self.imph - imh - textsize[1] - self.downPass)
        cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_0(self, padded, im, text, fontScale=1, thickness=3):
        im = cv2.resize(im, (1824, 940))
        imrh, imrw, c = im.shape
        self.padFromTheSide = int(self.impw / 2 - imrw / 2)

        padded[self.padFromAbove:self.padFromAbove + imrh, self.padFromTheSide:self.padFromTheSide + imrw, :] = im

        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((padded.shape[1] - textsize[0]) / 2)
        textY = int((70 + textsize[1]) / 2)
        cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_2(self, padded, im, text):
        im = cv2.resize(im, (910, 470))
        imh, imw, c = im.shape
        padded[-imh - self.downPass:-self.downPass, imw + self.padBetween: imw + self.padBetween + imw, :] = im
        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((imw + self.padBetween) + (imw - textsize[0]) / 2)
        textY = int(self.imph - self.fix_h - textsize[1] - self.downPass)
        cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_3(self, padded, im, text):
        im = cv2.resize(im, (910, 470))
        rdh, rdw, c = im.shape
        padded[-rdh - self.downPass:-self.downPass,
        2 * self.fix_w + 2 * self.padBetween: 2 * self.fix_w + 2 * self.padBetween + rdw, :] = im

        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
        textX = int((2 * self.fix_w + 2 * self.padBetween) + (self.fix_w - textsize[0]) / 2)
        textY = int(self.imph - self.fix_h - textsize[1] - self.downPass)
        cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_4(self, padded, im, text):
        im = cv2.resize(im, (910, 470))
        imh, imw, c = im.shape
        padded[-imh - self.downPass:-self.downPass, self.padFromTheSide:self.padFromTheSide + imw, :] = im

        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]

        textX = int((imw - textsize[0]) / 2)
        textY = int(self.imph - imh - textsize[1] - self.downPass)
        cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_5(self, padded, im, text):
        im = cv2.resize(im, (910, 470))
        imh, imw, c = im.shape
        padded[-imh - self.downPass:-self.downPass, imw + self.padBetween: imw + self.padBetween + imw, :] = im

        fontScale = 1
        thickness = 4
        textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]

        textX = int((imw + self.padBetween) + (imw - textsize[0]) / 2)

        textY = int(self.imph - imh - textsize[1] - self.downPass)
        cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color, thickness=thickness)
        return padded

    def set_screen_8(self, padded, im, text, fontScale=1, thickness=3, x=None, y=None):
        im = cv2.resize(im, (1824, 300))
        imh, imw, c = im.shape
        padFromTheSide = int(self.impw / 2 - imw / 2)

        # SCreen2
        text_down_pass = 20
        padded[-imh - self.downPass - text_down_pass:-self.downPass - text_down_pass,
        padFromTheSide: padFromTheSide + imw, :] = im

        if x is not None and y is not None:
            textsize = cv2.getTextSize(text, self.font, fontScale, thickness)[0]
            textX = int((padded.shape[1] - textsize[0]) / 2)
            textY = int(self.imph - imh - textsize[1] - self.downPass + 15 - text_down_pass)
            cv2.putText(padded, text, (textX, textY), self.font, fontScale=fontScale, color=self.color,
                        thickness=thickness)

            thickness = 2
            txt_size = 1.3
            textsize = cv2.getTextSize(text, self.font, txt_size, thickness)[0]
            textX = int((padded.shape[1] - textsize[0]) / 2)
            cv2.putText(padded, x, (textX, 1460), self.font, fontScale=txt_size, color=(255, 255, 255),
                        thickness=thickness)
            cv2.putText(padded, y, (7, 1283), self.font, fontScale=txt_size, color=(255, 255, 255), thickness=thickness)
        return padded

    @staticmethod
    def get_mode(screens):
        not_none = len([x for x in screens if x is not None])
        if 2 <= not_none <= 4:
            return not_none
        elif not_none < 2:
            return 2
        return 4

    def getIm(self, state, rd, rgbImg):
        self.p_c = None
        self.p_r = None

        self.scale_x = rgbImg.shape[1] / self.imw
        self.scale_y = rgbImg.shape[0] / self.imh

        self.full_res_rgb = rgbImg.copy()
        self.rgbImg = cv2.resize(rgbImg, (self.imw, self.imh))
        self.finalIm = np.copy(self.rgbImg)
        self.rd = rd

        # Setting Camera Off Mode
        if state.camera_off:
            for index, k in enumerate(state.screens):
                if "Camera" == k:
                    state.screens[index] = None
                elif "Fusion" == k:
                    state.screens[index] = None

        self.screens_output = {}

        # Gen Video Mode
        if self.fusion_frame is not None:
            self.screens_output["Fusion"] = self.fusion_frame

        if "Radar Perception" in state.screens:
            ## Deprecated
            veh_ret_im, self.cur_bb = self.radar_veh_net.getIm(self.rd, self.full_res_rgb.copy(),
                                                               state=state,
                                                               scale=(self.scale_x, self.scale_y))
            self.screens_output["Radar Vehicle"] = veh_ret_im
            road_ret_im, road_probs = self.radar_road_net.getIm(self.rd, self.screens_output["Radar Vehicle"],
                                                                state=state,
                                                                scale=(self.scale_x, self.scale_y))
            self.screens_output["Radar Road"] = road_ret_im

            self.screens_output["Radar Ped"] = self.radar_ped_net.getIm(self.rd, road_ret_im,
                                                                        state=state,
                                                                        scale=(self.scale_x, self.scale_y))[0]

            self.screens_output["Radar Perception"] = self.screens_output["Radar Road"]

        if "Radar Road Vehicle" in state.screens:
            self.screens_output["Radar Road Vehicle"] = self.radar_road_vehicle.getIm(self.rd, self.full_res_rgb.copy(),
                                                                                      state=state,
                                                                                      seg_instance=state.seg_instance)
            self.p_r = self.radar_road_vehicle.probs

        if "Radar Ped MF" in state.screens:
            if "Radar Ped MF" in self.temporal_features_in:
                x_t = self.temporal_features_in["Radar Ped MF"]
            else:
                x_t = None

            ret_im, pef_mf_probs, x_t = self.radar_ped_net_mf.getIm(self.rd, x_t=x_t,
                                                                    imToDraw=self.full_res_rgb.copy(),
                                                                    state=state,
                                                                    scale=(self.scale_x, self.scale_y))
            self.temporal_features_out["Radar Ped MF"] = x_t
            # print("Manager: Put new features")
            self.screens_output["Radar Ped MF"] = ret_im

        if "Combined RVP" in state.screens:
            self.screens_output["Combined RVP"] = self.combined_model.getIm(self.rd, self.full_res_rgb.copy(),
                                                                            state=state,
                                                                            seg_instance=state.seg_instance)

            self.screens_output["Combined RVP"] = self.radar_ped_net.getIm(self.rd, self.screens_output["Combined RVP"],
                                                                           state=state,

                                                                           scale=(self.scale_x, self.scale_y))[0]

            if state.crop_image:
                mask = np.zeros(self.screens_output["Combined RVP"].shape, dtype=self.screens_output["Combined RVP"].dtype)
                mask[:, state.crop_image:-state.crop_image, :] = self.screens_output["Combined RVP"][:, state.crop_image:-state.crop_image]
                self.screens_output["Combined RVP"] = mask
                # self.screens_output["Combined RVP"] = self.screens_output["Combined RVP"][:, state.crop_image:-state.crop_image]

            self.p_r = self.combined_model.road_model.probs

        # if "Radar Ped" in state.screens:
        #     self.screens_output["Radar Ped"] = self.radar_ped_net.getIm(self.rd, self.full_res_rgb.copy(),
        #                                                                 state=state,
        #                                                                 scale=(self.scale_x, self.scale_y))[0]

        # Run Camera
        if "Camera" in state.screens:
            self.screens_output["Camera"] = self.camera_multi_seg.getIm(self.rgbImg, self.finalIm, state=state)
            if state.od_flag:
                self.screens_output["Camera"] = self.camera_od.getIm(self.rgbImg, self.screens_output["Camera"],
                                                                     state=state)
            self.p_c = self.camera_multi_seg.probs

        if self.frame_data is not None:
            for i in range(len(self.frame_data)):
                x1 = int(self.frame_data[i]['bbox'][0] * 1 / self.scale_x)
                y1 = int(self.frame_data[i]['bbox'][1] * 1 / self.scale_y)
                x2 = int(self.frame_data[i]['bbox'][2] * 1 / self.scale_x)
                y2 = int(self.frame_data[i]['bbox'][3] * 1 / self.scale_y)
                if self.frame_data[i]['cat'] in self.classes_plt:
                    color = self.classes_plt[self.frame_data[i]['cat']]
                    cv2.rectangle(self.screens_output["Camera"], (x1, y1), (x2, y2), color, 1)

        # Run Fusion
        if self.p_r is not None and self.p_c is not None:
            self.screens_output["Fusion"] = self.getFusionMulti(self.p_r, self.p_c[:3, :, :], self.rgbImg)
        self.screens_output["Range Doppler"] = self.getRD(self.rd)

        # Range Azimuth
        # if self.radar_road_vehicle.process_array is not None:
        #     self.screens_output["Range Azimuth"] = self.getRA(self.radar_road_vehicle.process_array.cpu().numpy())
        # else:
        #     print("Processed array is None")

        # Gen Heatmaps
        # self.screens_output["Radar Combine Heatmap"] = self.radar_road_vehicle.get_heatmap_im(2)

        mode = self.get_mode(state.screens)

        if mode == 2:
            padded = np.zeros((1480, 2000, self.c), dtype=self.cur_dtype)
            self.imph, self.impw, self.c = padded.shape

            part = [index for index, i in enumerate(state.screens) if i is not None]

            if state.screens[0] is not None and len(part) > 0:
                cur_name = state.screens[part[0]]
                padded = self.set_screen_0(padded, self.screens_output[cur_name], self.titles[cur_name], fontScale=2,
                                           thickness=5)

            if state.screens[part[1]] is not None and len(part) > 1:
                cur_name = state.screens[part[1]]
                x = "Range"
                if cur_name == "Range Azimuth":
                    y = " Az"
                elif cur_name == "Range Doppler":
                    y = "Vel"
                else:
                    x, y = None, None
                padded = self.set_screen_8(padded, self.screens_output[cur_name], self.titles[cur_name], fontScale=2,
                                           thickness=5, x=x, y=y)

        elif mode == 4:
            padded = np.zeros((1600, 2950, self.c), dtype=self.cur_dtype)

            self.imph, self.impw, self.c = padded.shape

            if state.screens[1] is not None:
                cur_name = state.screens[1]
                padded = self.set_screen_1(padded, self.screens_output[cur_name], self.titles[cur_name])

            if state.screens[2] is not None:
                cur_name = state.screens[2]
                padded = self.set_screen_2(padded, self.screens_output[cur_name], self.titles[cur_name])

            if state.screens[3] is not None:
                cur_name = state.screens[3]
                padded = self.set_screen_3(padded, self.screens_output[cur_name], self.titles[cur_name])

            if state.screens[0] is not None:
                cur_name = state.screens[0]
                padded = self.set_screen_0(padded, self.screens_output[cur_name], self.titles[cur_name])

        elif mode == 3:
            padded = np.zeros((1600, 2000, self.c), dtype=self.cur_dtype)

            self.imph, self.impw, self.c = padded.shape

            part = [index for index, i in enumerate(state.screens) if i is not None]

            if state.screens[0] is not None and len(part) > 0:
                cur_name = state.screens[part[0]]
                padded = self.set_screen_0(padded, self.screens_output[cur_name], self.titles[cur_name])

            if state.screens[part[1]] is not None and len(part) > 1:
                cur_name = state.screens[part[1]]
                padded = self.set_screen_4(padded, self.screens_output[cur_name], self.titles[cur_name])
                # padded = self.set_screen_4(padded,
                #                            cv2.resize(self.screens_output[cur_name], None, fx=cur_scale, fy=cur_scale),
                #                            self.titles[cur_name])

            if state.screens[part[2]] is not None and len(part) > 2:
                cur_name = state.screens[part[2]]
                padded = self.set_screen_5(padded, self.screens_output[cur_name], self.titles[cur_name])

                # padded = self.set_screen_5(padded,
                #                            cv2.resize(self.screens_output[cur_name], None, fx=cur_scale, fy=cur_scale),
                #                            self.titles[cur_name])



        if state.recording:
            if os.path.exists(self.recording_path):
                cv2.imwrite(self.recording_path + "/" + str(state.timestamp) + ".png", padded)
                print("recorded")
            else:
                print("couldn't record")

        return padded
